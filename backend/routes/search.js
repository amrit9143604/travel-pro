const express = require("express");
const router = express.Router();
const { validateUserToken } = require("../middlewares/authUser");
const { searchDestination } = require("../controllers/searchDestination");

router.post("/users/search-destination", validateUserToken, searchDestination);

module.exports = router;
