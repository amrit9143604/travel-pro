const express = require("express");
const router = express.Router();
const {validateUserToken} = require("../middlewares/authUser");
const { addReview } = require("../controllers/reviewController");

router.post("/users/add-review", validateUserToken, addReview);


module.exports = router;
