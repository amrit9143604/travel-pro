const express = require("express");
const router = express.Router();
const { validateAdminToken } = require("../../middlewares/authAdmin");
const {
  addItinerary,
  getAdminItinerary,
  getAdminItineraryData,
  deleteAdminItinerary,
  updateAdminItinerary,
} = require("../../controllers/admin/itineraryController");

router.post("/admin/add-itinerary", validateAdminToken, addItinerary);
router.get("/admin/get-all-itinerary", validateAdminToken, getAdminItinerary);
router.post("/admin/get-itinerary", validateAdminToken, getAdminItineraryData);
router.delete(
  "/admin/delete-itinerary",
  validateAdminToken,
  deleteAdminItinerary
);
router.put("/admin/update-itinerary", validateAdminToken, updateAdminItinerary);

module.exports = router;
