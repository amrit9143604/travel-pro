const express = require("express");
const router = express.Router();
const { validateAdminToken } = require("../../middlewares/authAdmin");
const { addDestination, getDestination, getAllDestinations, deleteAdminDestination, updateAdminDestination} = require("../../controllers/admin/destinationController");


router.post("/admin/add-destination", validateAdminToken, addDestination);
router.post("/admin/get-destination", validateAdminToken, getDestination);
router.get("/admin/get-all-destination", validateAdminToken, getAllDestinations);
router.delete("/admin/delete-destination", validateAdminToken, deleteAdminDestination);
router.put("/admin/update-destination", validateAdminToken, updateAdminDestination);

module.exports = router;
