const express = require("express");
const router = express.Router();
const { validateAdminToken } = require("../../middlewares/authAdmin");
const { uploadFiles, upload, deleteObjectFromS3 } = require("../../controllers/admin/uploadController");

router.post("/admin/upload-files", validateAdminToken, upload, uploadFiles);
router.post("/admin/delete-files", validateAdminToken, deleteObjectFromS3);

module.exports = router;
