const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const { signUp, signIn, signOut } = require('../../controllers/admin/adminController');

router.post('/admin/signup',[
    check('email').isEmail().withMessage('Valid email is required'),
    check('password').isLength({ min: 6 }).withMessage('Password must be at least 6 chars long'),
], signUp);

router.post('/admin/signin',[
    check('email').isEmail().withMessage('Valid email is required'),
    check('password').isLength({ min: 6 }).withMessage('Password must be at least 6 chars long')
], signIn);

router.get('/admin/signout', signOut);

module.exports = router;