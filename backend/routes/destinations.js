const express = require("express");
const router = express.Router();
const { validateUserToken } = require("../middlewares/authUser");
const {getDestination, getAllDestinations} = require("../controllers/destinationController");



router.post("/users/get-destination", validateUserToken, getDestination);

router.get("/users/get-all-destinations", validateUserToken, getAllDestinations);

module.exports = router;
