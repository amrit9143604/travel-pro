const express = require("express");
const router = express.Router();
const { validateUserToken } = require("../middlewares/authUser");
const {
  addItinerary,
  getItinerary,
  deleteItinerary,
  updateItinerary,
  getAllItinerary,
  getAdminItinerary,
  getAdminItineraryData
} = require("../controllers/itineraryController");

router.post("/users/add-itinerary", validateUserToken, addItinerary);

router.post("/users/get-itinerary", validateUserToken, getItinerary);

router.delete("/users/delete-itinerary", validateUserToken, deleteItinerary);

router.put("/users/update-itinerary", validateUserToken, updateItinerary);

router.post("/users/get-all-itinerary", validateUserToken, getAllItinerary);
router.get("/users/get-all-admin-itinerary", validateUserToken, getAdminItinerary);
router.post("/users/get-admin-itinerary-data", validateUserToken, getAdminItineraryData);


module.exports = router;
 