const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const { signUp, signIn, signOut } = require('../controllers/userController');

router.post('/users/signup',[
    check('email').isEmail().withMessage('Valid email is required'),
    check('password').isLength({ min: 6 }).withMessage('Password must be at least 6 chars long'),
], signUp);

router.post('/users/signin',[
    check('email').isEmail().withMessage('Valid email is required'),
    check('password').isLength({ min: 6 }).withMessage('Password must be at least 6 chars long')
], signIn);

router.get('/users/signout', signOut);

module.exports = router;
