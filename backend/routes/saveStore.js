const express = require("express");
const router = express.Router();
const { validateUserToken } = require("../middlewares/authUser");
const {
  createSavedDestination,
  getSavedDestinations,
} = require("../controllers/saveDestinationController");
const {
  createSavedItinerary,
  getSavedItineraries,
} = require("../controllers/saveItineraryController");

router.post(
  "/users/save-destination",
  validateUserToken,
  createSavedDestination
);

router.post(
  "/users/get-saved-destinations",
  validateUserToken,
  getSavedDestinations
);

router.post("/users/save-itinerary", validateUserToken, createSavedItinerary);

router.post(
  "/users/get-saved-itineraries",
  validateUserToken,
  getSavedItineraries
);

module.exports = router;
