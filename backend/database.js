const mongoose = require("mongoose");

async function DbConnect() {
  try {
    const DB_URL = process.env.MONGODB_URL;
    await mongoose.connect(DB_URL);
    console.log("MongoDB connected...");
  } catch (error) {
    console.error("Connection error:", error);
  }
}

module.exports = DbConnect;
