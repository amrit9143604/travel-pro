const mongoose = require("mongoose");

const savedItinerarySchema = new mongoose.Schema({
    savedItinerariesList: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Itinerary",
        unique: true
    }],
    savedBy:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }
}, {timestamps: true});

module.exports = mongoose.model("savedItineary", savedItinerarySchema);