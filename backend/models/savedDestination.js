const mongoose = require("mongoose");

const savedDestination = new mongoose.Schema(
  {
    savedDestinationsList: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Destination",
        unique: true
      },
    ],
    savedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("savedDestination", savedDestination);
