const mongoose = require("mongoose");

const itineraryDetails = new mongoose.Schema({
  date: {
    type: Date,
    required: true,
  },
  dayDetails: {
    type: String,
    required: true,
  },
});

const itinerarySchema = mongoose.Schema({
  planName: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  travelStartDate: {
    type: Date,
    required: true,
  },
  travelEndDate: {
    type: Date,
    required: true,
  },
  travelMode: {
    type: String,
    required: true,
  },
  details: {
    type: [itineraryDetails],
    required: true,
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  estimatedCost: {
    type: Number,
    required: true,
  },
});

itinerarySchema.index({
  travelStartDate: 'text',
  travelEndDate: 'text',
  destination: 'text',
  estimatedCost: 'text'
});

module.exports = mongoose.model("itinerary", itinerarySchema);