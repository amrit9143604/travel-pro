const mongoose = require("mongoose");

const destinationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    landmarks: {
      type: Array,
      required: true,
    },
    latitude: {
      type: String,
      required: true,
    },
    longitude: {
      type: String,
      required: true,
    },
    state: {
      type: String,
      required: true,
      enum: [
        "Andhra Pradesh",
        "Arunachal Pradesh",
        "Assam",
        "Bihar",
        "Chhattisgarh",
        "Goa",
        "Gujarat",
        "Haryana",
        "Himachal Pradesh",
        "Jammu and Kashmir",
        "Jharkhand",
        "Karnataka",
        "Kerala",
        "Madhya Pradesh",
        "Maharashtra",
        "Manipur",
        "Meghalaya",
        "Mizoram",
        "Nagaland",
        "Odisha",
        "Punjab",
        "Rajasthan",
        "Sikkim",
        "Tamil Nadu",
        "Telangana",
        "Tripura",
        "Uttarakhand",
        "Uttar Pradesh",
        "West Bengal",
        "Andaman and Nicobar Islands",
        "Chandigarh",
        "Dadra and Nagar Haveli",
        "Daman and Diu",
        "Delhi",
        "Lakshadweep",
        "Puducherry",
        "Ladakh",
      ],
    },
    description: {
      type: String,
      required: true,
    },
    images: {
      type: Array,
      required: true,
    },
    avgTravelExpenses: {
      type: String,
      required: true,
    },
    attractions: {
      type: Array,
      required: true,
    },
    category: {
      type: String,
      required: true,
    },
    currentWeather: {
      type: String,
      required: true,
    },
    weatherIcon: {
      type: String,
      required: true,
    },
    temperature: {
      type: Number,
      required: true,
    },
    reviews: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Review",
      },
    ],
  },
  { timestamps: true }
);

destinationSchema.index({
  name: "text",
  landmarks: "text",
  attractions: "text",
  city: "text",
  state: "text",
  category: "text",
});

module.exports = mongoose.model("Destination", destinationSchema);

// Difference between saving data in objects vs arrays in mongodb

// Storing Data in Objects:

// When you store data in objects, you typically use key-value pairs where each key represents a field or attribute of your document.

// Storing Data in Arrays:

// Storing data in arrays involves grouping related data together within an array field.
// This approach is useful when dealing with collections of related data or when you need to store multiple values for a single field.
