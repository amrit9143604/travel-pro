const mongoose = require("mongoose");

const adminItineraryDetails = new mongoose.Schema({
  date: {
    type: Date,
    required: true,
  },
  dayDetails: {
    type: String,
    required: true,
  },
});

const adminItinerarySchema = mongoose.Schema({
  planName: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  travelStartDate: {
    type: Date,
    required: true,
  },
  travelEndDate: {
    type: Date,
    required: true,
  },
  travelMode: {
    type: String,
    required: true,
  },
  details: {
    type: [adminItineraryDetails],
    required: true,
  },
  estimatedCost: {
    type: Number,
    required: true,
  },
});

adminItinerarySchema.index({
  travelStartDate: 'text',
  travelEndDate: 'text',
  destination: 'text',
  estimatedCost: "text"
});


module.exports = mongoose.model("adminItinerary", adminItinerarySchema);