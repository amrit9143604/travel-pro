const Review = require("../models/review");
const Destination = require("../models/destination");
const User = require("../models/user");

const addReview = async (req, res) => {
  try {
    const { name, review, rating, destination_id, user_id } = req.body;
    
    if (
      !name ||
      !destination_id ||
      !user_id ||
      !review ||
      !rating ||
      destination_id.length != 24 ||
      user_id.length != 24
    ) {
      return res.status(400).json({
        error:
          "Invalid destination_id or user_id or review or rating not provided",
      });
    }

    //check the destination exists or not

    const destination = await Destination.findById(destination_id); // gives the whole result object
    if (!destination) {
      return res.status(400).json({
        error: "Destination does not exist, please check destinaion_id again!",
      });
    }

    // check the user exists or not

    const user = await User.findById(user_id); // gives the whole result object
    if (!user) {
      return res.status(400).json({
        error: "User does not exist, please check user_id again!",
      });
    }

    // check if the user already reviewed the destination or not

    const beforeReviewed = await Review.findOne({
      destination_id: destination_id,
      user_id: user_id,
    });
    if (beforeReviewed) {
      {
        return res.status(400).json({
          error:
            "You have already reviewed this destination, cannot review the same destination again!",
        });
      }
    }

    const reviews = new Review({
      name: name,
      review: review,
      rating: rating,
      destination_id: destination_id,
      user_id: user_id,
    });
    await reviews.save();
    destination.reviews.push(reviews);
    await destination.save();


    return res.status(200).json({
      message: "Review added successfully",
      review: reviews
    });
  } catch (error) {
    return res.status(500).json({
      error: "Error adding review, Internal server error!",
      error: error,
    });
  }
};

module.exports = { addReview };
