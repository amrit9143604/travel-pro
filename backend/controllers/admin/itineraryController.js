const AdminItinerary = require("../../models/adminItinerary");

const addItinerary = async (req, res) => {
  try {
    const {
      planName,
      destination,
      travelStartDate,
      travelEndDate,
      travelMode,
      details,
      estimatedCost,
      createdBy,
    } = req.body;

    if (travelStartDate > travelEndDate) {
      return res.status(400).json({
        error: "Travel start date cannot be greater than travel end date",
      });
    }

    const newItinerary = new AdminItinerary({
      planName: planName,
      destination: destination,
      travelStartDate: travelStartDate,
      travelEndDate: travelEndDate,
      travelMode: travelMode,
      details: details,
      createdBy: createdBy,
      estimatedCost: estimatedCost,
    });

    await newItinerary.save();

    return res.status(200).json({
      message: "Itinerary added successfully",
      savedItineary: newItinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error in adding itinerary, Internal server error!",
      error: error,
    });
  }
};

const getAdminItinerary = async (req, res) => {
  const allItinerary = await AdminItinerary.find();

  if (!allItinerary) {
    return res.status(200).json({
      message: "No itineraries created from you",
      data: [],
    });
  }

  return res.status(200).json({
    message: "Itineraries Fetched Succesfully",
    data: allItinerary,
  });
};

const getAdminItineraryData = async (req, res) => {
  try {
    const { itinerary_id } = req.body;
    const itinerary = await AdminItinerary.findById(itinerary_id);

    if (!itinerary) {
      return res.status(400).json({
        error: "Invalid Itinerary id or itinarary does not exist",
      });
    }

    return res.status(200).json({
      message: "Itinerary fetched successfully",
      data: itinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while getting itinerary, Internal server error!",
      error: error,
    });
  }
};

const deleteAdminItinerary = async (req, res) => {
  try {
    if (!req.body.itinerary_id || req.body.itinerary_id.length != 24) {
      return res.status(400).json({
        error: "Invalid Itinerary id or itinarary does not exist",
      });
    }
    const { itinerary_id } = req.body;
    const itinerary = await AdminItinerary.findByIdAndDelete(itinerary_id);

    return res.status(200).json({
      message: "Itinerary deleted successfully",
      data: itinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while deleting itinerary, Internal server error!",
      error: error,
    });
  }
};

const updateAdminItinerary = async (req, res) => {
  try {
    if (!req.body.itinerary_id || req.body.itinerary_id.length != 24) {
      return res.status(400).json({
        error: "Invalid Itinerary id or itinarary does not exist",
      });
    }
    const { itinerary_id } = req.body;
    const updatedData = req.body.updatedData; // Assuming updatedData contains the new itinerary data

    // Ensure that itinerary_id is valid

    // Find the itinerary by ID and update it with the new data
    const itinerary = await AdminItinerary.findByIdAndUpdate(
      itinerary_id,
      updatedData,
      { new: true }
    );

    // Check if itinerary exists
    if (!itinerary) {
      return res.status(404).json({
        error: "Itinerary not found",
      });
    }

    // Return the updated itinerary
    return res.status(200).json({
      message: "Itinerary updated successfully",
      data: itinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while updating itinerary, Internal server error!",
      error: error.message,
    });
  }
};

module.exports = {
  addItinerary,
  getAdminItinerary,
  getAdminItineraryData,
  deleteAdminItinerary,
  updateAdminItinerary,
};

// Difference between create and save method in mongoose

// Model.create():

// This is a static method of a Mongoose model.
// It allows you to create and save one or more documents in a single operation.
// You pass an array of objects or a single object to create documents.
// It returns a promise that resolves to an array of documents that were created.

// const UserModel = require('./models/user');

// UserModel.create({ name: 'John', age: 30 })
//   .then(createdUser => {
//     console.log(createdUser);
//   })
//   .catch(error => {
//     console.error(error);
//   });

//   document.save():

// This method is called on an instance of a Mongoose document.
// It is used to save the changes made to an existing document or to save a new document that was created using the constructor function of the model.
// It returns a promise that resolves to the saved document.

// const user = new UserModel({ name: 'John', age: 30 });

// user.save()
//   .then(savedUser => {
//     console.log(savedUser);
//   })
//   .catch(error => {
//     console.error(error);
//   });
