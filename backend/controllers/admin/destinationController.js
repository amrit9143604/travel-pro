const Destination = require("../../models/destination");
const Review = require("../../models/review");
const axios  = require('axios')


const addDestination = async (req, res) => {
  try {
    const weatherResponse = await axios.get(
      `https://weatherapi-com.p.rapidapi.com/current.json?q=${req.body.city}`,
      {
        headers: {
          "X-RapidAPI-Key": process.env.WEATHER_API_KEY,
          "X-RapidAPI-Host": process.env.WEATHER_API_HOST,
        },
      }
    );

    const destination = new Destination({
      name: req.body.name,
      city: req.body.city,
      landmarks: req.body.landmarks,
      latitude: weatherResponse.data.location.lat,
      longitude: weatherResponse.data.location.lon,
      state: req.body.state,
      description: req.body.description,
      images: req.body.images,
      avgTravelExpenses: req.body.avgTravelExpenses,
      attractions: req.body.attractions,
      category: req.body.category,
      currentWeather: weatherResponse.data.current.condition.text,
      weatherIcon: weatherResponse.data.current.condition.icon,
      temperature: weatherResponse.data.current.temp_c,
    });

    const savedDestination = await destination.save();

    res
      .status(200)
      .json({ message: "Destination added successfully", savedDestination });
  } catch (error) {
    return res.status(500).json({
      message: "Error in adding destination, Internal server error!",
      error: error,
    });
  }
};

const getDestination = async (req, res) => {
  try {
    if (!req.body.destination_id || req.body.destination_id.length != 24) {
      return res.status(400).json({
        error: "Invalid destination_id",
      });
    }

    const destination = await Destination.findById(req.body.destination_id);


    if (!destination) {
      return res.json(200).json({
        message: "No destination found",
      });
    }

    return res.status(200).json({
      message: "Destination fetched successfully",
      data: {
        destination
      },
    });
  } catch (err) {
    return res.status(500).json({
      message: "Error fetching destination, Internal server error!",
      error: err,
    });
  }
};

const getAllDestinations = async (req, res) => {
  try {
    const destinations = await Destination.find(
      {},
      {
        description: 0,
        currentWeather: 0,
        weatherIcon: 0,
        temperature: 0,
        reviews: 0,
        longitude: 0,
        latitude: 0,
      }
    );
    if (!destinations) {
      return res.status(200).json({
        message: "No destinations found",
      });
    }

    return res.status(200).json({
      message: "Destinations fetched successfully",
      data: destinations,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error fetching destinations, Internal server error!",
      error: error,
    });
  }
};

const deleteAdminDestination = async (req, res) => {
  try {
    if (!req.body.destination_id || req.body.destination_id.length != 24) {
      return res.status(400).json({
        error: "Invalid Destination id or destination does not exist",
      });
    }
    const { destination_id } = req.body;
    const destination = await Destination.findByIdAndDelete(destination_id);


    return res.status(200).json({
      message: "Destination deleted successfully",
      data: destination,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while deleting destination, Internal server error!",
      error: error,
    });
  }
};

const updateAdminDestination = async (req, res) => {
  try {

    // if (!req.body.destination_id || req.body.destination_id.length != 24) {
    //   return res.status(400).json({
    //     error: "Invalid Destination id or destination does not exist",
    //   });
    // }

    
    const { destination_id } = req.body;
    const updatedData = req.body.updatedData; // Assuming updatedData contains the new itinerary data

    // Ensure that itinerary_id is valid
    if (!destination_id) {
      return res.status(400).json({
        error: "Invalid Destination Id or destination does not exist",
      });
    } 

    // Find the itinerary by ID and update it with the new data
    const destination = await Destination.findByIdAndUpdate(
        destination_id,
      updatedData,
      { new: true }
    );

    // Check if itinerary exists
    if (!destination) {
      return res.status(404).json({
        error: "Destination not found",
      });
    }

    // Return the updated itinerary
    return res.status(200).json({
      message: "Destination updated successfully",
      data: destination,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while updating destination, Internal server error!",
      error: error.message,
    });
  }
};

module.exports = { addDestination, getDestination, getAllDestinations, deleteAdminDestination, updateAdminDestination };

// Difference between create and save method in mongoose

// Model.create():

// This is a static method of a Mongoose model.
// It allows you to create and save one or more documents in a single operation.
// You pass an array of objects or a single object to create documents.
// It returns a promise that resolves to an array of documents that were created.

// const UserModel = require('./models/user');

// UserModel.create({ name: 'John', age: 30 })
//   .then(createdUser => {
//     console.log(createdUser);
//   })
//   .catch(error => {
//     console.error(error);
//   });

//   document.save():

// This method is called on an instance of a Mongoose document.
// It is used to save the changes made to an existing document or to save a new document that was created using the constructor function of the model.
// It returns a promise that resolves to the saved document.

// const user = new UserModel({ name: 'John', age: 30 });

// user.save()
//   .then(savedUser => {
//     console.log(savedUser);
//   })
//   .catch(error => {
//     console.error(error);
//   });
