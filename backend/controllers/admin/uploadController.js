const multer = require("multer");
const { S3Client, PutObjectCommand, DeleteObjectCommand } = require("@aws-sdk/client-s3");

const s3Client = new S3Client({
  region: process.env.REGION,
  credentials: {
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
  },
});

const upload = multer({
  storage: multer.memoryStorage(),
}).array("files", 10); // 'files' is the field name for multiple files, 10 is the maximum number of files

const uploadFiles = async (req, res, next) => {
  try {
    const files = req.files;
    if (!files || files.length === 0) {
      return res.status(400).json({ error: "No files uploaded" });
    }

    const uploadPromises = files.map(async (file) => {
      const uploadParams = {
        Bucket: process.env.BUCKET_NAME,
        Key: `${file.originalname}`, // Using the original filename as the object key
        Body: file.buffer, // binary data of the file that has been uploaded.
        ContentType: file.mimetype,
      };

      const data = await s3Client.send(new PutObjectCommand(uploadParams));
      return `https://${uploadParams.Bucket}.s3.amazonaws.com/${uploadParams.Key}`;
    });

    const uploadedFilesUrls = await Promise.all(uploadPromises);

    res.json({
      message: "Files uploaded successfully",
      filesUrls: uploadedFilesUrls,
    });
  } catch (err) {
    return res.status(500).json({
      message: "Error uploading files, Internal server error!",
      error: err,
    });
  }
};

const deleteObjectFromS3 = async (req, res) => {
  try {
    const keys = req.body.keys; // Assuming keys are provided in the request body as an array
    if (!keys || !Array.isArray(keys) || keys.length === 0) {
      return res.status(400).json({ error: "No file names given or invalid format" });
    }

    const deletePromises = keys.map(async (key) => {
      const params = {
        Bucket: process.env.BUCKET_NAME,
        Key: key,
      };
      await s3Client.send(new DeleteObjectCommand(params));
      console.log(`Deleted object: ${key}`);
    });

    await Promise.all(deletePromises);

    return res.status(200).json({ message: "Objects deleted successfully" });
  } catch (err) {
    console.error("Error deleting objects:", err);
    return res.status(500).json({ error: "Internal server error" });
  }
};


module.exports = { upload, uploadFiles, deleteObjectFromS3 };
