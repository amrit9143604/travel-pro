const jwt = require("jsonwebtoken");
const CryptoJS = require("crypto-js"); // hashing the password
const Admin = require("../../models/admin"); // importing models
const { validationResult } = require("express-validator");

const generateJwtToken = (_id, name, email, password) => {
  return jwt.sign({ _id, name, email, password }, process.env.SECRET_KEY);
};

const signUp = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array[0].msg,
        param: errors.array[0].param,
      });
    }

    const admin = new Admin(req.body);
    admin.password = CryptoJS.AES.encrypt(
      admin.password,
      process.env.SECRET_KEY
    ).toString();

    const savedAdmin = await admin.save();

    const token = generateJwtToken(
      savedAdmin._id,
      savedAdmin.email,
      savedAdmin.password
    );

    res.cookie("token", token, { expire: new Date() + 9999 }); // putting the token in the cookie
    res.json({ admin: savedAdmin, token: token });
  } catch (err) {
    if (err.code === 11000) {
      return res.status(400).json({
        error: `Error while saving Admin, Duplicate '${
          Object.keys(err.keyValue)[0]
        }' found`,
      });
    } else {
      return res.status(500).json({
        error: "Error while creating admin, Internal server error",
      });
    }
  }
};

const signIn = async (req, res) => {
  try {
    const { email, password } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        //  Error 422 is an HTTP code that tells you that the server can't process your request, although it understands it
        error: errors.array()[0].msg,
        param: errors.array()[0].param,
      });
    }

    const admin = await Admin.findOne({ email }).exec();
    if (!admin) {
      return res.status(400).json({
        error: "No admin exists with this email",
      });
    }

    const bytes = CryptoJS.AES.decrypt(admin.password, process.env.SECRET_KEY);
    const originalPassword = bytes.toString(CryptoJS.enc.Utf8);

    if (originalPassword !== password) {
      return res.status(401).json({
        error: "Wrong Credentials, Please try again!",
      });
    }

    const token = jwt.sign(
      {
        userID: admin._id,
        name: admin.name,
        password: admin.password,
        email: admin.email,
        role: "Admin",
      },
      process.env.SECRET_KEY
    );

    // Put token in cookie
    res.cookie("token", token, { expire: new Date() + 9999 });

    return res.status(200).json({
      token,
      admin: admin,
    });
  } catch (err) {
    return res.status(500).json({
      error: "Error in creating admin, Internal server error!",
    });
  }
};

const signOut = (req, res) => {
  res.clearCookie("token");
  res.status(200).json({
    message: "Admin logout successfully",
  });
};

module.exports = { signUp, signIn, signOut };
