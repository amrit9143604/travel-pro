const AdminItinerary = require("../models/adminItinerary");
const Itinerary = require("../models/itinerary");

const addItinerary = async (req, res) => {
  try {
    const {
      planName,
      destination,
      travelStartDate,
      travelEndDate,
      travelMode,
      details,
      estimatedCost,
      createdBy,
    } = req.body;

    if (travelStartDate > travelEndDate) {
      return res.status(400).json({
        error: "Travel start date cannot be greater than travel end date",
      });
    }

    const newItinerary = new Itinerary({
      planName: planName,
      destination: destination,
      travelStartDate: travelStartDate,
      travelEndDate: travelEndDate,
      travelMode: travelMode,
      details: details,
      createdBy: createdBy,
      estimatedCost: estimatedCost,
    });

    await newItinerary.save();
    return res.status(200).json({
      message: "Itinerary added successfully",
      savedItineary: newItinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while adding Itinerary, Internal Server Error!",
      error: error,
    });
  }
};

const getItinerary = async (req, res) => {
  try {
    if (!req.body.itinerary_id || req.body.itinerary_id.length != 24) {
      return res.status(400).json({
        error: "Invalid itinerary_id",
      });
    }
    const { itinerary_id } = req.body;
    const itinerary = await Itinerary.findById(itinerary_id);
    

    if (!itinerary) {
      return res.status(400).json({
        message: "No itinerary found",
      });
    }
    return res.status(200).json({
      message: "Itinerary fetched successfully",
      data: itinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while getting itinerary, Internal server error!",
      error: error,
    });
  }
};

const deleteItinerary = async (req, res) => {
  try {
    if (!req.body.itinerary_id || req.body.itinerary_id.length != 24) {
      return res.status(400).json({
        error: "Invalid Itinerary id or itinarary does not exist",
      });
    }
    const { itinerary_id } = req.body;
    const itinerary = await Itinerary.findByIdAndDelete(itinerary_id);

    return res.status(200).json({
      message: "Itinerary deleted successfully",
      data: itinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while deleting itinerary, Internal server error!",
      error: error,
    });
  }
};

const updateItinerary = async (req, res) => {
  try {
    if (!req.body.itinerary_id || req.body.itinerary_id.length != 24) {
      return res.status(400).json({
        error: "Invalid Itinerary id or itinarary does not exist",
      });
    }
    const { itinerary_id } = req.body;
    const updatedData = req.body.updatedData; // Assuming updatedData contains the new itinerary data

    // Ensure that itinerary_id is valid

    // Find the itinerary by ID and update it with the new data
    const itinerary = await Itinerary.findByIdAndUpdate(
      itinerary_id,
      updatedData,
      { new: true }
    );

    // Check if itinerary exists
    if (!itinerary) {
      return res.status(404).json({
        error: "Itinerary not found",
      });
    }

    // Return the updated itinerary
    return res.status(200).json({
      message: "Itinerary updated successfully",
      data: itinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while updating itinerary, Internal server error!",
      error: error.message,
    });
  }
};

const getAllItinerary = async (req, res) => {
  if (!req.body.user_id || req.body.user_id.length != 24) {
    return res.status(400).json({
      error: "Invalid user_id",
    });
  }
  const { user_id } = req.body;
  const allItinerary = await Itinerary.find({ createdBy: user_id });

  if (!allItinerary) {
    return res.status(200).json({
      message: "No itineraries created from you",
      data: [],
    });
  }

  return res.status(200).json({
    message: "Itineraries Fetched Succesfully",
    data: allItinerary,
  });
};

const getAdminItinerary = async (req, res) => {
  const allItinerary = await AdminItinerary.find();

  if (!allItinerary) {
    return res.status(200).json({
      message: "No itineraries created from you",
      data: [],
    });
  }

  return res.status(200).json({
    message: "Itineraries Fetched Succesfully",
    data: allItinerary,
  });
};

const getAdminItineraryData = async (req, res) => {
  try {
    const { itinerary_id } = req.body;
    const itinerary = await AdminItinerary.findById(itinerary_id);

    if (!itinerary) {
      return res.status(400).json({
        error: "Invalid Itinerary id or itinarary does not exist",
      });
    }

    return res.status(200).json({
      message: "Itinerary fetched successfully",
      data: itinerary,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Error while getting itinerary, Internal server error!",
      error: error,
    });
  }
};

module.exports = {
  addItinerary,
  getItinerary,
  deleteItinerary,
  updateItinerary,
  getAllItinerary,
  getAdminItinerary,
  getAdminItineraryData,
};
