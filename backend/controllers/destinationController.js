const Destination = require("../models/destination");
const Review = require("../models/review");

const getDestination = async (req, res) => {
  try {
    if (!req.body.destination_id || req.body.destination_id.length !== 24) {
      return res.status(400).json({
        error: "Invalid destination_id",
      });
    }

    const destination = await Destination.findById(req.body.destination_id);
    const reviews = await Review.find({
      destination_id: req.body.destination_id,
    });

    if (!destination) {
      return res.status(200).json({
        message: "No destination found",
      });
    }

    // Sorts an array of reviews in descending order
    reviews.sort((a, b) => {
      return b.createdAt - a.createdAt;
    });

    return res.status(200).json({
      message: "Destination fetched successfully",
      data: { 
        destination,
        reviews,
      }, 
    });
  } catch (err) {
    return res.status(500).json({
      message: "Error fetching destination, Internal server error!",
      error: err,
    });
  }
};


const getAllDestinations = async (req, res) => {
    const destinations = await Destination.find({},
      {
        description: 0,
        currentWeather: 0,
        weatherIcon: 0,
        temperature: 0,
        reviews: 0,
        longitude: 0,
        latitude: 0,
      }
    );
    if (!destinations) {
      return res.status(200).json({
        message: "No destinations found",
      });
    }

    return res.status(200).json({
      message: "Destinations fetched successfully",
      data: destinations,
    });
};

module.exports = { getDestination, getAllDestinations };
