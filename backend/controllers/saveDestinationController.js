const SavedDestination = require("../models/savedDestination");
const Destination = require("../models/destination");

const dropIndexes = async () => {
  try {
    await SavedDestination.collection.dropIndexes();
  } catch (err) {
    console.error("Error dropping indexes:", err);
  }
};

const createSavedDestination = async (req, res) => {
  try {
    const { destination_id, user_id } = req.body;
    if (
      !destination_id ||
      destination_id.length != 24 ||
      !user_id ||
      user_id.length != 24
    ) {
      return res.status(400).json({
        // 400 status code - the server cannot or will not process the request due to something that is perceived to be a client error
        error: "Destination ID required",
      });
    }
    const savedDestination = await SavedDestination.findOne({
      savedBy: user_id,
    });

    if (savedDestination) {
      const index =
        savedDestination.savedDestinationsList.indexOf(destination_id); // find the index of the first occurrence of a specified value within an array.

      if (index > -1) {
        savedDestination.savedDestinationsList.splice(index, 1);
        await savedDestination.save();
        res
          .status(200)
          .json({ message: "Removed from your saved destination list", savedDestination:{} });
        // in spilce method
        // The first argument, index, indicates the index at which to start changing the array.
        // The second argument, 1, indicates how many elements should be removed.

        // if index == -1
        // So, in this case, since -1 is less than 0, splice() will not remove any elements, and the array will remain unchanged.
      } else {
        savedDestination.savedDestinationsList.push(destination_id);
        await savedDestination.save();
        res
          .status(200)
          .json({ message: "Added to your saved destination list", savedDestination: savedDestination });
      }
    } else {
      const newSavedDestination = new SavedDestination({
        savedDestinationsList: [destination_id],
        savedBy: user_id,
      });
      await newSavedDestination.save();
      res.status(200).json({
        message: "Added to your saved destination list",
        newSavedDestination,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "Error in saving destination, Internal server error!",
      error: error,
    });
  }
};


const getSavedDestinations = async (req, res) => {
  try {
    const { user_id } = req.body;
    if (!user_id || user_id.length != 24) {
      return res.status(400).json({
        message: "User ID required",
      });
    }
    const savedDestination = await SavedDestination.findOne({
      savedBy: user_id,
    });
    if (savedDestination) {
      const destination = await Destination.find({
        _id: { $in: savedDestination.savedDestinationsList },
      }); // specifies that the documents returned should have an _id field that matches any of the values in the savedDestination.savedDestinationsList array.

      res.status(200).json({ destinations: destination });
    } else {
      res.status(200).json({ destinations: [] });
    }
  } catch (err) {
    res.status(500).json({
      message: "Error getting the destinations, Internal server error!",
    });
  }
};

module.exports = { dropIndexes, createSavedDestination, getSavedDestinations };
