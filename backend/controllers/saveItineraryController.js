const SavedItinerary = require("../models/savedItinerary");
const Itinerary = require("../models/itinerary");
const AdminItinerary = require("../models/adminItinerary");

const dropIndexes = async () => {
  try {
    await SavedItinerary.collection.dropIndexes();
  } catch (err) {
    console.error("Error dropping indexes:", err);
  }
};


const createSavedItinerary = async (req, res) => {
  try {
    const { itinerary_id, user_id } = req.body;
    if (
      !itinerary_id ||
      itinerary_id.length != 24 ||
      !user_id ||
      user_id.length != 24
    ) {
      return res.status(400).json({
        // 400 status code - the server cannot or will not process the request due to something that is perceived to be a client error
        error: "Itinerary ID or user ID required",
      });
    }
    const savedItinerary = await SavedItinerary.findOne({
      savedBy: user_id,
    });

    if (savedItinerary) {
      const index = savedItinerary.savedItinerariesList.indexOf(itinerary_id); // find the index of the first occurrence of a specified value within an array.

      if (index > -1) {
        savedItinerary.savedItinerariesList.splice(index, 1);
        await savedItinerary.save();
        res
          .status(200)
          .json({
            message: "Removed from your saved itinerary list",
            savedItinerary: {},
          });
        // in spilce method
        // The first argument, index, indicates the index at which to start changing the array.
        // The second argument, 1, indicates how many elements should be removed.

        // if index == -1
        // So, in this case, since -1 is less than 0, splice() will not remove any elements, and the array will remain unchanged.
      } else {
        savedItinerary.savedItinerariesList.push(itinerary_id);
        await savedItinerary.save();
        res
          .status(200)
          .json({
            message: "Added to your saved itinerary list",
            savedItinerary: savedItinerary,
          });
      }
    } else {
      const newSavedItinerary = new SavedItinerary({
        savedItinerariesList: [itinerary_id],
        savedBy: user_id,
      });
      await newSavedItinerary.save();
      res.status(200).json({
        message: "Added to your saved itinerary list",
        newSavedItinerary,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "Error in saving itinerary, Internal server error!",
      error: error,
    });
  }
};



const getSavedItineraries = async (req, res) => {
  try {
    const { user_id } = req.body;
    if (!user_id || user_id.length != 24) {
      return res.status(400).json({
        message: "User ID required",
      });
    }
    const savedItinerary = await SavedItinerary.findOne({
      savedBy: user_id,
    });
    if (savedItinerary) {
      const itinerary = await AdminItinerary.find({
        _id: { $in: savedItinerary.savedItinerariesList },
      }); // specifies that the documents returned should have an _id field that matches any of the values in the savedDestination.savedDestinationsList array.

      res.status(200).json({ itineraries: itinerary });
    } else {
      res.status(200).json({ itineraries: [] });
    }
  } catch (err) {
    res.status(500).json({
      message: "Error getting the itineraries, Internal server error!",
    });
  }
};

module.exports = { dropIndexes, createSavedItinerary, getSavedItineraries };

// It splits the destinationName, cityName, state, and category properties by spaces and pushes the resulting words into an array called keywords.
// It iterates over the attractions array within the current destination object, and for each attraction, it pushes the attraction into the keywords array.
// It iterates over the landmarks array within the current destination object, and for each landmark, it pushes the landmark into the keywords array.
