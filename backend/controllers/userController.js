const jwt = require("jsonwebtoken");
const CryptoJS = require("crypto-js"); // hashing the password
const User = require("../models/user"); // importing models
const { validationResult } = require("express-validator");

const generateJwtToken = (_id, username, email, password) => {
  return jwt.sign({ _id, username, email, password, role: "User" }, process.env.SECRET_KEY);
};

const signUp = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array[0].msg,
        param: errors.array[0].param,
      });
    }

    const user = new User(req.body);
    user.password = CryptoJS.AES.encrypt(
      user.password,
      process.env.SECRET_KEY
    ).toString();

    const savedUser = await user.save();

    const token = generateJwtToken(
      savedUser._id,
      savedUser.username,
      savedUser.email,
      savedUser.password,
    );

    const expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 1); // Setting expiration time to 1 day from now

    res.cookie("token", token, { expires: expirationDate, httpOnly: true });

    res.json({ user: savedUser, token: token });
  } catch (err) {
    if (err.code === 11000) {
      return res.status(400).json({
        error: `Error while saving User, Duplicate '${
          Object.keys(err.keyValue)[0]
        }' found`,
      });
    } else {
      return res.status(500).json({
        message: "Error in creating user, Internal server error!",
      });
    }
  }
};

const signIn = async (req, res) => {
  try {
    const { email, password } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        //  Error 422 is an HTTP code that tells you that the server can't process your request, although it understands it
        error: errors.array()[0].msg,
        param: errors.array()[0].param,
      });
    }

    const user = await User.findOne({ email }).exec();
    if (!user) {
      return res.status(400).json({
        error: "No user exists with this username or email",
      });
    }

    const bytes = CryptoJS.AES.decrypt(user.password, process.env.SECRET_KEY);
    const originalPassword = bytes.toString(CryptoJS.enc.Utf8);

    if (originalPassword !== password) {
      return res.status(401).json({
        error: "Wrong Credentials, Please try again!",
      });
    }

    const token = jwt.sign(
      {
        userID: user._id,
        username: user.username,
        password: user.password,
        email: user.email,
        role: "User",
      },
      process.env.SECRET_KEY
    );

    // Put token in cookie
    const expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 1); // Setting expiration time to 1 day from now

    res.cookie("token", token, { expires: expirationDate });

    return res.status(200).json(
      {
        token,
        user: user,
      },
    );
  } catch (err) {
    console.log('Err: ', err);
    return res.status(500).json({
      message: "Error while signing user, Internal server error",
    });
  }
};

const signOut = (req, res) => {
  res.clearCookie("token");
  res.json({
    message: "User logout successfully",
  });
};

module.exports = { signUp, signIn, signOut };
