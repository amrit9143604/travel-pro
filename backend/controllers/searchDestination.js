const Destination = require("../models/destination");

const searchDestination = async (req, res) => {
  try {
    const { query } = req.body;
    const searchResults = await Destination.find({
      $text: { $search: query },
    });

    res.status(200).json({
      results: searchResults,
    });
  } catch (error) {
    res.status(500).json({
      message: "Error searching the destination, Internal server error!",
      error: error,
    });
  }
};

module.exports = { searchDestination };
