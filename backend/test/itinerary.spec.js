const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../app");
const Itinerary = require("../models/itinerary");
const jwt = require("jsonwebtoken");
const AdminItinerary = require("../models/adminItinerary");

chai.use(chaiHttp);
const expect = chai.expect;

const mockItinerary = {
  planName: "Hyderabad",
  destination: "Manali",
  travelStartDate: "2024-02-15",
  travelEndDate: "2024-02-19",
  travelMode: "Flight",
  details: [
    {
      date: "2024-02-15",
      dayDetails: "Solang Valley",
    },
    {
      date: "2024-02-16",
      dayDetails: "Hidimba Devi Temple",
    },
    {
      date: "2024-02-17",
      dayDetails: "Jogini Waterfall",
    },
  ],
  createdBy: "65cef88d0147f9b1f826b2ea",
  estimatedCost: 20000,
};

const mockAdminItinerary = {
  planName: "Hyderabad",
  destination: "Manali",
  travelStartDate: "2024-02-15",
  travelEndDate: "2024-02-19",
  travelMode: "Flight",
  details: [
    {
      date: "2024-02-15",
      dayDetails: "Solang Valley",
    },
    {
      date: "2024-02-16",
      dayDetails: "Hidimba Devi Temple",
    },
    {
      date: "2024-02-17",
      dayDetails: "Jogini Waterfall",
    },
  ],
  estimatedCost: 20000,
};

describe("Itinerary Routes", () => {
  afterEach(() => {
    sinon.restore();
  });

  beforeEach(() => {
    const jwtStub = sinon.stub(jwt, "verify");
    jwtStub.callsFake((token, secret, callback) => {
      // Mocking a decoded token with role "Admin"
      const decodedToken = { role: "User" };
      callback(null, decodedToken);
    });
  });

  it("should return 400 if travel start date is greater than travel end date", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/add-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({
        planName: "Hyderabad",
        destination: "Manali",
        travelStartDate: "2024-02-19",
        travelEndDate: "2024-02-15",
        travelMode: "Flight",
        details: [
          {
            date: "2024-02-15",
            dayDetails: "Solang Valley",
          },
          {
            date: "2024-02-16",
            dayDetails: "Hidimba Devi Temple",
          },
          {
            date: "2024-02-17",
            dayDetails: "Jogini Waterfall",
          },
        ],
        createdBy: "65cef88d0147f9b1f826b2ea",
        estimatedCost: 20000,
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should create a new itinerary and return the itinerary", (done) => {
    const itinararyModelMock = {};
    Object.assign(Itinerary, itinararyModelMock);
    sinon.stub(Itinerary.prototype, "save").resolves(mockItinerary);
    chai
      .request(app)
      .post("/api/v1/users/add-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({
        planName: "Hyderabad",
        destination: "Manali",
        travelStartDate: "2024-02-15",
        travelEndDate: "2024-02-19",
        travelMode: "Flight",
        details: [
          {
            date: "2024-02-15",
            dayDetails: "Solang Valley",
          },
          {
            date: "2024-02-16",
            dayDetails: "Hidimba Devi Temple",
          },
          {
            date: "2024-02-17",
            dayDetails: "Jogini Waterfall",
          },
        ],
        createdBy: "65cef88d0147f9b1f826b2ea",
        estimatedCost: 20000,
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("Give 500 when something unusual happens", (done) => {
    const itinararyModelMock = {};
    Object.assign(Itinerary, itinararyModelMock);
    sinon.stub(Itinerary.prototype, "save").rejects(new Error("Test Error"));
    chai
      .request(app)
      .post("/api/v1/users/add-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({
        planName: "Hyderabad",
        destination: "Manali",
        travelStartDate: "2024-02-15",
        travelEndDate: "2024-02-19",
        travelMode: "Flight",
        details: [
          {
            date: "2024-02-15",
            dayDetails: "Solang Valley",
          },
          {
            date: "2024-02-16",
            dayDetails: "Hidimba Devi Temple",
          },
          {
            date: "2024-02-17",
            dayDetails: "Jogini Waterfall",
          },
        ],
        createdBy: "65cef88d0147f9b1f826b2ea",
        estimatedCost: 20000,
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });

  it("should return 400 if itinerary_id is missing or invalid", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/get-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 with itinerary if itinerary exists", (done) => {
    const itineraryModelMock = {
      findById: () => Promise.resolve(mockItinerary),
    };
    // Override the User with the mock
    Object.assign(Itinerary, itineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        console.log(res.body);
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 400 with itinerary if itinerary does not exists", (done) => {
    const itineraryModelMock = {
      findById: () => Promise.resolve(null),
    };
    // Override the User with the mock
    Object.assign(Itinerary, itineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        console.log(res.body);
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const itineraryModelMock = {
      findById: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Itinerary, itineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });

  it("should return 400 if itinerary_id is missing or invalid when deleting a itinerary", (done) => {
    chai
      .request(app)
      .delete("/api/v1/users/delete-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 when itinerary is deleted successfully", (done) => {
    const itineraryModelMock = {
      findByIdAndDelete: () => Promise.resolve(mockItinerary),
    };
    // Override the User with the mock
    Object.assign(Itinerary, itineraryModelMock);

    chai
      .request(app)
      .delete("/api/v1/users/delete-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const itineraryModelMock = {
      findByIdAndDelete: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Itinerary, itineraryModelMock);

    chai
      .request(app)
      .delete("/api/v1/users/delete-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });

  it("should return 400 if itinerary_id is missing or invalid when updating a itinerary", (done) => {
    chai
      .request(app)
      .put("/api/v1/users/update-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 404 if itinerary does not exist", (done) => {
    const itinararyModelMock = {
      findByIdAndUpdate: () => Promise.resolve(null),
    };

    // Override the User with the mock
    Object.assign(Itinerary, itinararyModelMock);

    chai
      .request(app)
      .put("/api/v1/users/update-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fb" })
      .end((err, res) => {
        expect(res).to.have.status(404);
        done();
      });
  });

  it("should return 200 when itinerary is updated successfully", (done) => {
    const itineraryModelMock = {
      findByIdAndUpdate: () => Promise.resolve(mockItinerary),
    };
    // Override the User with the mock
    Object.assign(Itinerary, itineraryModelMock);

    chai
      .request(app)
      .put("/api/v1/users/update-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const itineraryModelMock = {
      findByIdAndUpdate: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Itinerary, itineraryModelMock);

    chai
      .request(app)
      .put("/api/v1/users/update-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });

  it("should return 400 if user_id is missing or invalid", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/get-all-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 with all itineraries if they exist", (done) => {
    const itinararyModelMock = {
      find: () => Promise.resolve(mockItinerary),
    };

    // Override the Destination model with the mock
    Object.assign(Itinerary, itinararyModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-all-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ user_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 200 with message if no itineraries exist", (done) => {
    const itinararyModelMock = {
      find: () => Promise.resolve(null),
    };

    // Override the Destination model with the mock
    Object.assign(Itinerary, itinararyModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-all-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({ user_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 200 with all itineraries if they exist", (done) => {
    const itinararyModelMock = {
      find: () => Promise.resolve(mockAdminItinerary),
    };
    Object.assign(AdminItinerary, itinararyModelMock);

    chai
      .request(app)
      .get("/api/v1/users/get-all-admin-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 200 with message if no itineraries exist", (done) => {
    const itinararyModelMock = {
      find: () => Promise.resolve(null),
    };

    Object.assign(AdminItinerary, itinararyModelMock);

    chai
      .request(app)
      .get("/api/v1/users/get-all-admin-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 400 if itinerary_id is missing or invalid", (done) => {
    const itineraryModelMock = {
      findById: () => Promise.resolve(null),
    };
    Object.assign(AdminItinerary, itineraryModelMock);
    chai
      .request(app)
      .post("/api/v1/users/get-admin-itinerary-data")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        console.log(res.body);
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 with itinerary if itinerary exists", (done) => {
    const itineraryModelMock = {
      findById: () => Promise.resolve(mockAdminItinerary),
    };
    // Override the User with the mock
    Object.assign(AdminItinerary, itineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-admin-itinerary-data")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const itineraryModelMock = {
      findById: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(AdminItinerary, itineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-admin-itinerary-data")
      .set("Authorization", "Bearer mockedToken")
      .send({ itinerary_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });
});
