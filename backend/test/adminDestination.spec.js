const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../app");
const jwt = require("jsonwebtoken");
const Destination = require("../models/destination");

chai.use(chaiHttp);
const expect = chai.expect;

const mockAdminDestination = {
  name: "Gateway of India",
  city: "Mumbai",
  landmarks: ["Gateway of India", "Marine Drive"],
  state: "Maharashtra",
  description:
    "India's financial capital, known for Bollywood, diverse culture, and bustling streets",
  images: [
    "https://travel-pro.s3.amazonaws.com/goi.jpeg",
    "https://travel-pro.s3.amazonaws.com/goi_landmarks.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive_attractions.jpeg",
    "https://travel-pro.s3.amazonaws.com/inter.jpeg",
  ],
  avgTravelExpenses: "1500 - 3000",
  attractions: ["Elephanta Caves", "Siddhivinayak Temple", "street food"],
  category: "Urban Exploration",
};

describe("Admin Destination Routes", () => {
  afterEach(() => {
    sinon.restore();
  });

  beforeEach(() => {
    const jwtStub = sinon.stub(jwt, "verify");
    jwtStub.callsFake((token, secret, callback) => {
      // Mocking a decoded token with role "Admin"
      const decodedToken = { role: "Admin" };
      callback(null, decodedToken);
    });
  });

  it("should create a new destination and return the destinaiton", (done) => {
    const destinationModelMock = {};
    Object.assign(Destination, destinationModelMock);
    sinon.stub(Destination.prototype, "save").resolves(mockAdminDestination);
    chai
      .request(app)
      .post("/api/v1/admin/add-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({
        name: "Gateway of India",
  city: "Mumbai",
  landmarks: ["Gateway of India", "Marine Drive"],
  state: "Maharashtra",
  description:
    "India's financial capital, known for Bollywood, diverse culture, and bustling streets",
  images: [
    "https://travel-pro.s3.amazonaws.com/goi.jpeg",
    "https://travel-pro.s3.amazonaws.com/goi_landmarks.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive_attractions.jpeg",
    "https://travel-pro.s3.amazonaws.com/inter.jpeg",
  ],
  avgTravelExpenses: "1500 - 3000",
  attractions: ["Elephanta Caves", "Siddhivinayak Temple", "street food"],
  category: "Urban Exploration",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("Give 500 when something unusual happens", (done) => {
    const destinationModelMock = {};
    Object.assign(Destination, destinationModelMock);
    sinon
      .stub(Destination.prototype, "save")
      .rejects(new Error("Test Error"));
    chai
      .request(app)
      .post("/api/v1/admin/add-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({
        planName: "Hyderabad",
        destination: "Manali",
        travelStartDate: "2024-02-15",
        travelEndDate: "2024-02-19",
        travelMode: "Flight",
        details: [
          {
            date: "2024-02-15",
            dayDetails: "Solang Valley",
          },
          {
            date: "2024-02-16",
            dayDetails: "Hidimba Devi Temple",
          },
          {
            date: "2024-02-17",
            dayDetails: "Jogini Waterfall",
          },
        ],
        estimatedCost: 20000,
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });

  it("should return 400 if destination_id is missing or invalid", (done) => {
    chai
      .request(app)
      .post("/api/v1/admin/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const destinationModelMock = {
      findById: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });

  it("should return 400 if destination_id is missing or invalid when deleting a destination", (done) => {
    chai
      .request(app)
      .delete("/api/v1/admin/delete-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 when destination is deleted successfully", (done) => {
    const destinaitonModelMock = {
      findByIdAndDelete: () => Promise.resolve(mockAdminDestination),
    };
    // Override the User with the mock
    Object.assign(Destination, destinaitonModelMock);

    chai
      .request(app)
      .delete("/api/v1/admin/delete-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const destinaitonModelMock = {
      findByIdAndDelete: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Destination, destinaitonModelMock);

    chai
      .request(app)
      .delete("/api/v1/admin/delete-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });

  it("should return 400 if destination_id is missing or invalid when updating a destination", (done) => {
    chai
      .request(app)
      .put("/api/v1/admin/update-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        console.log(res);
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 404 if destination does not exist", (done) => {
    const destinationModelMock = {
      findByIdAndUpdate: () => Promise.resolve(null),
    };

    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .put("/api/v1/admin/update-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fb" })
      .end((err, res) => {
        expect(res).to.have.status(404);
        done();
      });
  });

  it("should return 200 when destination is updated successfully", (done) => {
    const destinationModelMock = {
      findByIdAndUpdate: () => Promise.resolve(mockAdminDestination),
    };
    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .put("/api/v1/admin/update-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        console.log(res.body);
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const destinationModelMock = {
      findByIdAndUpdate: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .put("/api/v1/admin/update-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });

  it("should return 200 with all destinations if they exist", (done) => {
    const destinationModelMock = {
      find: () => Promise.resolve(mockAdminDestination),
    };
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .get("/api/v1/admin/get-all-destination")
      .set("Authorization", "Bearer mockedToken")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 200 with message if no destinations exist", (done) => {
    const destinationModelMock = {
      find: () => Promise.resolve(null),
    };

    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .get("/api/v1/admin/get-all-destination")
      .set("Authorization", "Bearer mockedToken")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const destinationModelMock = {
      find: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .get("/api/v1/admin/get-all-destination")
      .set("Authorization", "Bearer mockedToken")
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });
  

  it("should return 200 with destination if destination exists", (done) => {
    const destinationModelMock = {
      findById: () => Promise.resolve(mockAdminDestination),
    };
    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65ccb0ea0cc3c0a6fd08d65b" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 200 with destination if destination exists", (done) => {
    const destinationModelMock = {
      findById: () => Promise.resolve(null),
    };
    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65ccb0ea0cc3c0a6fd08d65b" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const destinationModelMock = {
      findById: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });
});
