const chai = require("chai");
const chaiHttp = require("chai-http");
const { describe, it } = require("mocha");
const app = require("../app"); // Replace with the actual path
const Admin = require("../models/admin"); // Assuming you have the userModel file
const sinon = require("sinon");
const CryptoJS = require("crypto-js");

chai.use(chaiHttp);
const expect = chai.expect;

const mockAdmin = {
  email: "hello@gmail.com",
  name: "hello",
  password: "123456",
};

describe("Admin Signin Routes", () => {
  it("should login a admin and return a token and the admin", (done) => {
    const adminModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(mockAdmin),
      }),
    };

    // Override the User with the mock
    Object.assign(Admin, adminModelMock);

    sinon
      .stub(CryptoJS.AES, "decrypt")
      .returns({ toString: () => "invalidPassword" });

    chai
      .request(app)
      .post("/api/v1/admin/signin")
      .send({
        email: "invalidAdmin@gmail.com",
        password: "invalidPassword",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        expect(response.body).to.have.property("token");
        sinon.restore();
        done();
      });
  });

  it("Give 422 when the valid email or password not provided", (done) => {
    const adminModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(mockAdmin),
      }),
    };

    // Override the User with the mock
    Object.assign(Admin, adminModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/signin")
      .send({
        email: "",
        password: "",
      })
      .end((err, response) => {
        expect(response).to.have.status(422);
        done();
      });
  });

  it("Give 401 when the credentials are wrong", (done) => {
    const adminModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(mockAdmin),
      }),
    };

    // Override the User with the mock
    Object.assign(Admin, adminModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/signin")
      .send({
        email: "invalidAdmin@gmail.com",
        password: "invalidPassword",
      })
      .end((err, response) => {
        expect(response).to.have.status(401);
        sinon.restore();
        done();
      });
  });

  it("should return 400 with a duplicate key error message when a duplicate key error occurs during user signup", (done) => {
    const adminModelMock = {};
    Object.assign(Admin, adminModelMock);
    
    // Stub the save method of User model to reject with a duplicate key error
    sinon.stub(Admin.prototype, "save").rejects({
      code: 11000, // Simulate a duplicate key error
      keyValue: { email: "hello@gmail.com" } // Simulate the duplicated key
    });

    chai
      .request(app)
      .post("/api/v1/admin/signup")
      .send({
        username: "amrit",
        email: "hello@gmail.com", // Simulating a duplicated email
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(400);
        done();
      });
  });

  it("Give 400 when the admin does not exist", (done) => {
    const adminModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(null),
      }),
    };

    // Override the User with the mock
    Object.assign(Admin, adminModelMock);
    sinon.stub(CryptoJS.AES, "decrypt").returns({ toString: () => "1234567" });

    chai
      .request(app)
      .post("/api/v1/admin/signin")
      .send({
        email: "amrit@gmail.com",
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(400);
        sinon.restore();
        done();
      });
  });

  it("Give 500 when something unusual happens", (done) => {
    const adminModelMock = {
      findOne: () => ({
        exec: () => Promise.reject(),
      }),
    };

    // Override the User with the mock
    Object.assign(Admin, adminModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/signin")
      .send({
        email: "amrit@gmail.com",
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        sinon.restore();
        done();
      });
  });
});

describe("Signup Routes", () => {
  afterEach(() => {
    sinon.restore();
  });
  it("should create a new admin and return a token and the admin", (done) => {
    const adminModelMock = {};
    Object.assign(Admin, adminModelMock);
    sinon.stub(Admin.prototype, "save").resolves(mockAdmin);
    chai
      .request(app)
      .post("/api/v1/admin/signup")
      .send({
        name: "amrit",
        email: "amrit@gmail.com",
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        expect(response.body).to.have.property("token");
        done();
      });
  });

  it("Give 500 when something unusual happens", (done) => {
    const adminModelMock = {};
    Object.assign(Admin, adminModelMock);

    chai
      .request(app)
      .post("/api/v1/admin/signup")
      .send({
        name: "",
        email: "",
        password: "",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });
});

describe("Signout Routes", () => {
  afterEach(() => {
    sinon.restore();
  });
  it("should signout the user", (done) => {
    const adminModelMock = {};
    Object.assign(Admin, adminModelMock);
    chai
      .request(app)
      .get("/api/v1/admin/signout")
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });
});
