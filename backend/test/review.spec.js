const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../app");
const Destination = require("../models/destination");
const Review = require("../models/review");
const User = require("../models/user");
const jwt = require("jsonwebtoken");
const AdminItinerary = require("../models/adminItinerary");

chai.use(chaiHttp);
const expect = chai.expect;

const mockReview = {
  name: "amrit",
  review: "Nice place",
  rating: 5,
  destination_id: "65cfc52dbb35218b4df9fb28",
  user_id: "65cefc1177bae95d3a913868",
};

const mockDestination = {
  name: "Gateway of India",
  city: "Mumbai",
  landmarks: ["Gateway of India", "Marine Drive"],
  state: "Maharashtra",
  description:
    "India's financial capital, known for Bollywood, diverse culture, and bustling streets",
  images: [
    "https://travel-pro.s3.amazonaws.com/goi.jpeg",
    "https://travel-pro.s3.amazonaws.com/goi_landmarks.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive_attractions.jpeg",
    "https://travel-pro.s3.amazonaws.com/inter.jpeg",
  ],
  avgTravelExpenses: "1500 - 3000",
  attractions: ["Elephanta Caves", "Siddhivinayak Temple", "street food"],
  category: "Urban Exploration",
};

const mockUser = {
  email: "hello@gmail.com",
  username: "hello",
  password: "123456",
};

describe("Review Routes", () => {
  afterEach(() => {
    sinon.restore();
  });

  beforeEach(() => {
    const jwtStub = sinon.stub(jwt, "verify");
    jwtStub.callsFake((token, secret, callback) => {
      const decodedToken = { role: "User" };
      callback(null, decodedToken);
    });
  });

  it("should return 400 if invalid parameters are provided", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/add-review")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 400 if destination does not exist", (done) => {
    const destinationModelMock = {
      findById: () => Promise.resolve(null),
    };

    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/add-review")
      .set("Authorization", "Bearer mockedToken")
      .send({
        name: "amrit",
        review: "Nice place",
        rating: 5,
        destination_id: "65cefbf41befd992b72146fa",
        user_id: "65cefc1177bae95d3a913868",
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 400 if user does not exist", (done) => {
    const destinationModelMock = {
      findById: () => Promise.resolve(mockDestination),
    };

    const userModelMock = {
      findById: () => Promise.resolve(null),
    };

    Object.assign(Destination, destinationModelMock);
    Object.assign(User, userModelMock);

    chai
      .request(app)
      .post("/api/v1/users/add-review")
      .set("Authorization", "Bearer mockedToken")
      .send({
        name: "amrit",
        review: "Nice place",
        rating: 5,
        destination_id: "65cefbf41befd992b72146fa",
        user_id: "65cefc1177bae95d3a913868",
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body.error).to.equal(
          "User does not exist, please check user_id again!"
        );
        done();
      });
  });

  it("should return 400 if user has already reviewed the destination", (done) => {
    const destinationModelMock = {
      findById: () => Promise.resolve(mockDestination),
    };

    const userModelMock = {
      findById: () => Promise.resolve(mockUser),
    };

    const reviewModelMock = {
      findOne: (query) => {
        if (
          query.destination_id === "65cefbf41befd992b72146fa" &&
          query.user_id === "65cefc1177bae95d3a913868"
        ) {
          // Simulate that a review already exists for this destination and user
          return Promise.resolve(mockReview);
        } else {
          // Simulate that no review exists for this destination and user
          return Promise.resolve(null);
        }
      },
    };

    Object.assign(Destination, destinationModelMock);
    Object.assign(User, userModelMock);
    Object.assign(Review, reviewModelMock);

    chai
      .request(app)
      .post("/api/v1/users/add-review")
      .set("Authorization", "Bearer mockedToken")
      .send({
        name: "amrit",
        review: "Nice place",
        rating: 5,
        destination_id: "65cefbf41befd992b72146fa",
        user_id: "65cefc1177bae95d3a913868",
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body.error).to.equal(
          "You have already reviewed this destination, cannot review the same destination again!"
        );
        done();
      });
  });

  it("should add a new review and return it", (done) => {
    // Mock Review model
    const reviewModelMock = {};
    Object.assign(Review, reviewModelMock);
    sinon.stub(Review.prototype, "save").resolves(mockReview);

    // Mock destination object
    const destination = {
      reviews: [],
      save: sinon.stub().resolves(),
    };

    sinon.stub(Destination, "findById").resolves(destination);

    chai
      .request(app)
      .post("/api/v1/users/add-review")
      .set("Authorization", "Bearer mockedToken")
      .send({
        name: "John",
        review: "Great place!",
        rating: 5,
        destination_id: "65cfc52dbb35218b4df9fb27",
        user_id: "65cefc1177bae95d3a913867",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("should return 500 when something unusual happens", (done) => {
    const reviewModelMock = {
      findById: () => Promise.reject(new Error("Test Error")),
    };
    // Override the User with the mock
    Object.assign(Review, reviewModelMock);

    chai
      .request(app)
      .post("/api/v1/users/add-review")
      .set("Authorization", "Bearer mockedToken")
      .send({
        name: "John",
        review: "Great place!",
        rating: 5,
        destination_id: "65cfc52dbb35218b4df9fb27",
        user_id: "65cefc1177bae95d3a913867",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });
});
