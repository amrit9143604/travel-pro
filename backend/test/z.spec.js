const jwt = require("jsonwebtoken");
const { expect } = require("chai");
const sinon = require("sinon");
const { validateUserToken } = require("../middlewares/authUser");
const { validateAdminToken } = require("../middlewares/authAdmin");

describe("validateUserToken middleware", () => {
  let req, res, next;

  beforeEach(() => {
    req = {
      headers: {},
    };
    res = {
      status: sinon.stub().returnsThis(),
      json: sinon.spy(),
    };
    next = sinon.spy();
  });

  afterEach(() => {
    sinon.restore();
  });

  it("should return 403 status if authorization header is not provided", () => {
    validateUserToken(req, res, next);

    expect(res.status.calledWithExactly(403)).to.be.true;
    expect(
      res.json.calledWithExactly({
        error: "Not able to identify the token or token not provided",
      })
    ).to.be.true;
    expect(next.called).to.be.false;
  });

  it("should return 403 status if token is not provided in authorization header", () => {
    req.headers.authorization = "Bearer";

    validateUserToken(req, res, next);

    expect(res.status.calledWithExactly(403)).to.be.true;
    expect(
      res.json.calledWithExactly({
        error: "Not able to identify the token or token not provided",
      })
    ).to.be.true;
    expect(next.called).to.be.false;
  });

  it('should return 401 status if token role is not "User"', () => {
    req.headers.authorization = "Bearer someinvalidtoken";
    jwt.verify = sinon.stub().callsArgWith(2, null, { role: "Admin" });

    validateUserToken(req, res, next);

    expect(next.called).to.be.false;
    expect(res.status.calledWithExactly(401)).to.be.true;
    expect(
      res.json.calledWithExactly({
        message: "Unauthorized! Please login as a user",
      })
    ).to.be.true;
  });

  it("should return 401 status if jwt.verify throws an error", () => {
    req.headers.authorization = "Bearer someinvalidtoken";
    jwt.verify = sinon
      .stub()
      .callsArgWith(2, new Error("JWT verification failed"));

    validateUserToken(req, res, next);

    expect(next.called).to.be.false;
    expect(res.status.calledWithExactly(401)).to.be.true;
    expect(
      res.json.calledWithExactly({
        message: "Unauthorized! Please login as a user",
      })
    ).to.be.true;
  });
});

describe("validateAdminToken middleware", () => {
  let req, res, next;

  beforeEach(() => {
    req = {
      headers: {},
    };
    res = {
      status: sinon.stub().returnsThis(),
      json: sinon.spy(),
    };
    next = sinon.spy();
  });

  afterEach(() => {
    sinon.restore();
  });

  it("should return 403 status if authorization header is not provided", () => {
    validateAdminToken(req, res, next);

    expect(res.status.calledWithExactly(403)).to.be.true;
    expect(
      res.json.calledWithExactly({
        error: "Not able to identify the token or token not provided",
      })
    ).to.be.true;
    expect(next.called).to.be.false;
  });

  it("should return 403 status if token is not provided in authorization header", () => {
    req.headers.authorization = "Bearer";

    validateAdminToken(req, res, next);

    expect(res.status.calledWithExactly(403)).to.be.true;
    expect(
      res.json.calledWithExactly({
        error: "Not able to identify the token or token not provided",
      })
    ).to.be.true;
    expect(next.called).to.be.false;
  });

  it('should return 401 status if token role is not "Admin"', () => {
    req.headers.authorization = "Bearer someinvalidtoken";
    jwt.verify = sinon.stub().callsArgWith(2, null, { role: "User" });
  
    validateAdminToken(req, res, next);
  
    expect(next.called).to.be.false;
    expect(res.status.calledWithExactly(401)).to.be.true;
    expect(
      res.json.calledWithExactly({
        message: "Unauthorized! Please login as an admin",
      })
    ).to.be.true;
  });
  
});
