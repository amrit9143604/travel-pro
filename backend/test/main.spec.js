const chai = require("chai");
const chaiHttp = require("chai-http");
const { describe, it } = require("mocha");
const app = require("../app"); // Replace with the actual path
chai.use(chaiHttp);
const expect = chai.expect;

describe("Main server running", () => {
  it("Get the server running",  (done) => {
    chai
      .request(app)
      .get("/")
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });
});
