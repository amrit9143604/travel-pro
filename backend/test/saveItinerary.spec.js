const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../app");
const Itinerary = require("../models/itinerary");
const jwt = require("jsonwebtoken");
const SavedItinerary = require("../models/savedItinerary");
const { dropIndexes } = require("../controllers/saveItineraryController");

chai.use(chaiHttp);
const expect = chai.expect;
const mockItinerary = {
  planName: "Hyderabad",
  destination: "Manali",
  travelStartDate: "2024-02-15",
  travelEndDate: "2024-02-19",
  travelMode: "Flight",
  details: [
    {
      date: "2024-02-15",
      dayDetails: "Solang Valley",
    },
    {
      date: "2024-02-16",
      dayDetails: "Hidimba Devi Temple",
    },
    {
      date: "2024-02-17",
      dayDetails: "Jogini Waterfall",
    },
  ],
  createdBy: "65cef88d0147f9b1f826b2ea",
  estimatedCost: 20000,
};

const mockSaved = {
  savedItinerariesList: [],
  savedBy: "65c24ef3db0c2cc49cfb8d26",
};

describe("Save Itinerary Routes", () => {
  afterEach(() => {
    sinon.restore();
  });

  beforeEach(() => {
    const jwtStub = sinon.stub(jwt, "verify");
    jwtStub.callsFake((token, secret, callback) => {
      // Mocking a decoded token with role "Admin"
      const decodedToken = { role: "User" };
      callback(null, decodedToken);
    });
  });

  it("should return 400 if itinerary or user_id is missing or invalid", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/save-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should toggle save itinerary successfully if itinerary is already saved", (done) => {
    const SavedItineraryModelMock = {
      findOne: () =>
        Promise.resolve({
          savedItinerariesList: ["65c3279a468833978b960af4"],
          save: sinon.stub().resolves(),
        }),
    };

    Object.assign(SavedItinerary, SavedItineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/save-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({
        itinerary_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("should toggle save itinerary successfully if itinerary is already saved", (done) => {
    const SavedItineraryModelMock = {
      findOne: () =>
        Promise.resolve({
          savedItinerariesList: [],
          save: sinon.stub().resolves(),
        }),
    };

    Object.assign(SavedItinerary, SavedItineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/save-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({
        itinerary_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("should save new itinerary successfully if it's not saved before", (done) => {
    const SavedItineraryModelMock = {
      findOne: () => Promise.resolve(null),
    };
    const savedItineraryInstanceMock = { save: sinon.stub().resolves() };

    Object.assign(SavedItinerary, SavedItineraryModelMock);
    sinon
      .stub(SavedItinerary.prototype, "save")
      .resolves(savedItineraryInstanceMock);

    chai
      .request(app)
      .post("/api/v1/users/save-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({
        itinerary_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("should handle errors gracefully", (done) => {
    const SavedItineraryModelMock = {
      findOne: () => Promise.reject(new Error("Test Error")),
    };

    Object.assign(SavedItinerary, SavedItineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/save-itinerary")
      .set("Authorization", "Bearer mockedToken")
      .send({
        itinerary_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });

  it("should return 400 if user_id is missing or invalid while getting the saved itineraries", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/get-saved-itineraries")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return saved itineraries if they exist", (done) => {
    // Mock SavedDestination.findOne method
    const savedItineraryMock = {
      savedItinerariesList: ["itinerary_id1", "itinerary_id2"], // Example saved destinations
    };
    const SavedItineraryModelMock = {
      findOne: sinon.stub().resolves(savedItineraryMock), // Resolve with the mockSavedDestination object
    };
    Object.assign(SavedItinerary, SavedItineraryModelMock);

    // Mock Destination.find method
    const itineraryMock = [{ name: "Itinerary 1" }, { name: "Itinerary 2" }]; // Example destinations
    const ItineraryModelMock = {
      find: sinon.stub().resolves(itineraryMock), // Resolve with the mock destinations
    };
    Object.assign(Itinerary, ItineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-itineraries")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return saved itineraries if they exist", (done) => {
    const SavedItineraryModelMock = {
      findOne: sinon.stub().resolves(mockSaved),
    };
    Object.assign(SavedItinerary, SavedItineraryModelMock);
    const ItineraryModelMock = {
      find: sinon.stub().resolves(mockItinerary),
    };
    Object.assign(Itinerary, ItineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-itineraries")
      .set("Authorization", "Bearer mockedToken")
      .send({
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return an empty array if no itineraries are saved", (done) => {
    // Mock SavedDestination.findOne method to return null
    const SavedItineraryModelMock = {
      findOne: sinon.stub().resolves(null), // Resolve with null, indicating no saved destinations
    };
    Object.assign(SavedItinerary, SavedItineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-itineraries")
      .set("Authorization", "Bearer mockedToken")
      .send({
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should handle errors gracefully", (done) => {
    const SavedItineraryModelMock = {
      findOne: () => Promise.reject(new Error("Test Error")),
    };

    Object.assign(SavedItinerary, SavedItineraryModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-itineraries")
      .set("Authorization", "Bearer mockedToken")
      .send({
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });

  it("should log an error if dropping indexes fails", async () => {
    sinon
      .stub(SavedItinerary.collection, "dropIndexes")
      .throws(new Error("Drop indexes error"));
    const consoleErrorSpy = sinon.spy(console, "error");

    await dropIndexes();

    expect(consoleErrorSpy.calledWithMatch(/Error dropping indexes:/)).to.be
      .true;
  });
});
