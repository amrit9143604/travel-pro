const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../app");
const jwt = require("jsonwebtoken");
const { S3Client } = require("@aws-sdk/client-s3");

chai.use(chaiHttp);
const expect = chai.expect;

describe("Upload Routes", () => {
  afterEach(() => {
    sinon.restore();
  });

  beforeEach(() => {
    const jwtStub = sinon.stub(jwt, "verify");
    jwtStub.callsFake((token, secret, callback) => {
      // Mocking a decoded token with role "Admin"
      const decodedToken = { role: "Admin" };
      callback(null, decodedToken);
    });
  });

  it("should return 400 if files are not selected", (done) => {
    chai
      .request(app)
      .post("/api/v1/admin/upload-files")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 if files are selected", async () => {
    // Stub the behavior of S3Client send method to resolve promises
    const s3ClientStub = sinon.stub(S3Client.prototype, "send").resolves({});

    const res = await chai
      .request(app)
      .post("/api/v1/admin/upload-files")
      .set("Authorization", "Bearer mockedToken")
      .attach(
        "files",
        "/home/suyal.a/Desktop/Travel Pro/frontend/assets/destination.png"
      );
    expect(res).to.have.status(200);

    // Restore the stub after test  
    s3ClientStub.restore();
  });

  it("should return 500 if no files are selected", (done) => {
    const s3ClientStub = sinon
      .stub(S3Client.prototype, "send")
      .rejects(new Error("Test Error"));

    chai
      .request(app)
      .post("/api/v1/admin/upload-files")
      .set("Authorization", "Bearer mockedToken")
      .attach(
        "files",
        "/home/suyal.a/Desktop/Travel Pro/frontend/assets/destination.png"
      )
      .end((err, res) => {
        expect(res).to.have.status(500);
        s3ClientStub.restore();
        done();
      });
  });

  it("should return 400 if files are not selected", (done) => {
    chai
      .request(app)
      .post("/api/v1/admin/delete-files")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 if files are selected", async () => {
    // Stub the behavior of S3Client send method to resolve promises
    const s3ClientStub = sinon.stub(S3Client.prototype, "send").resolves({});

    const res = await chai
      .request(app)
      .post("/api/v1/admin/delete-files")
      .set("Authorization", "Bearer mockedToken")
      .send(
        {keys: ["key1/png", "key2.png"]}
      );
    expect(res).to.have.status(200);

    // Restore the stub after test
    s3ClientStub.restore();
  });

  it("should return 500 if no files are selected", (done) => {
    const s3ClientStub = sinon
      .stub(S3Client.prototype, "send")
      .rejects(new Error("Test Error"));

    chai
    .request(app)
    .post("/api/v1/admin/delete-files")
    .set("Authorization", "Bearer mockedToken")
    .send(
      {keys: ["key1/png", "key2.png"]}
    )
      .end((err, res) => {
        expect(res).to.have.status(500);
        s3ClientStub.restore();
        done();
      });
  });
});
