const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../app");
const Destination = require("../models/destination");
const Review = require("../models/review");
const User = require("../models/user");
const jwt = require("jsonwebtoken");
const SavedDestination = require("../models/savedDestination");
const { dropIndexes } = require("../controllers/saveDestinationController");


chai.use(chaiHttp);
const expect = chai.expect;

const mockReview = {
  name: "amrit",
  review: "Nice place",
  rating: 5,
  destination_id: "65cfc52dbb35218b4df9fb28",
  user_id: "65cefc1177bae95d3a913868",
};

const mockDestination = {
  name: "Gateway of India",
  city: "Mumbai",
  landmarks: ["Gateway of India", "Marine Drive"],
  state: "Maharashtra",
  description:
    "India's financial capital, known for Bollywood, diverse culture, and bustling streets",
  images: [
    "https://travel-pro.s3.amazonaws.com/goi.jpeg",
    "https://travel-pro.s3.amazonaws.com/goi_landmarks.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive_attractions.jpeg",
    "https://travel-pro.s3.amazonaws.com/inter.jpeg",
  ],
  avgTravelExpenses: "1500 - 3000",
  attractions: ["Elephanta Caves", "Siddhivinayak Temple", "street food"],
  category: "Urban Exploration",
};

const mockUser = {
  email: "hello@gmail.com",
  username: "hello",
  password: "123456",
};

const mockSaved = {
  savedDestinationsList: [],
  savedBy: "65c24ef3db0c2cc49cfb8d26",
};

describe("Save Destination Routes", () => {
  afterEach(() => {
    sinon.restore();
  });

  beforeEach(() => {
    const jwtStub = sinon.stub(jwt, "verify");
    jwtStub.callsFake((token, secret, callback) => {
      // Mocking a decoded token with role "Admin"
      const decodedToken = { role: "User" };
      callback(null, decodedToken);
    });
  });
  it("should return 400 if destination_id or user_id is missing or invalid", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/save-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should toggle save destination successfully if destination is already saved", (done) => {
    const SavedDestinationModelMock = {
      findOne: () =>
        Promise.resolve({
          savedDestinationsList: ["65c3279a468833978b960af4"],
          save: sinon.stub().resolves(),
        }),
    };

    Object.assign(SavedDestination, SavedDestinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/save-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({
        destination_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("should toggle save destination successfully if destination is already saved", (done) => {
    const SavedDestinationModelMock = {
      findOne: () =>
        Promise.resolve({
          savedDestinationsList: [],
          save: sinon.stub().resolves(),
        }),
    };

    Object.assign(SavedDestination, SavedDestinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/save-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({
        destination_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("should save new destination successfully if it's not saved before", (done) => {
    const SavedDestinationModelMock = {
      findOne: () => Promise.resolve(null),
    };
    const savedDestinationInstanceMock = { save: sinon.stub().resolves() };

    Object.assign(SavedDestination, SavedDestinationModelMock);
    sinon
      .stub(SavedDestination.prototype, "save")
      .resolves(savedDestinationInstanceMock);

    chai
      .request(app)
      .post("/api/v1/users/save-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({
        destination_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });

  it("should handle errors gracefully", (done) => {
    const SavedDestinationModelMock = {
      findOne: () => Promise.reject(new Error("Test Error")),
    };

    Object.assign(SavedDestination, SavedDestinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/save-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({
        destination_id: "65c3279a468833978b960af4",
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });

  it("should return 400 if user_id is missing or invalid while getting the saved destinations", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/get-saved-destinations")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return saved destinations if they exist", (done) => {
    // Mock SavedDestination.findOne method
    const savedDestinationMock = {
      savedDestinationsList: ["destination_id1", "destination_id2"], // Example saved destinations
    };
    const SavedDestinationModelMock = {
      findOne: sinon.stub().resolves(savedDestinationMock), // Resolve with the mockSavedDestination object
    };
    Object.assign(SavedDestination, SavedDestinationModelMock);

    // Mock Destination.find method
    const destinationMock = [
      { name: "Destination 1" },
      { name: "Destination 2" },
    ]; // Example destinations
    const DestinationModelMock = {
      find: sinon.stub().resolves(destinationMock), // Resolve with the mock destinations
    };
    Object.assign(Destination, DestinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-destinations")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return saved destinations if they exist", (done) => {
    // Mock SavedDestination.findOne method

    const SavedDestinationModelMock = {
      findOne: sinon.stub().resolves(mockSaved),
    };
    Object.assign(SavedDestination, SavedDestinationModelMock);
    const DestinationModelMock = {
      find: sinon.stub().resolves(mockDestination),
    };
    Object.assign(Destination, DestinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-destinations")
      .set("Authorization", "Bearer mockedToken")
      .send({
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return an empty array if no destinations are saved", (done) => {
    // Mock SavedDestination.findOne method to return null
    const SavedDestinationModelMock = {
      findOne: sinon.stub().resolves(null), // Resolve with null, indicating no saved destinations
    };
    Object.assign(SavedDestination, SavedDestinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-destinations")
      .set("Authorization", "Bearer mockedToken")
      .send({
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should handle errors gracefully", (done) => {
    const SavedDestinationModelMock = {
      findOne: () => Promise.reject(new Error("Test Error")),
    };

    Object.assign(SavedDestination, SavedDestinationModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-saved-destinations")
      .set("Authorization", "Bearer mockedToken")
      .send({
        user_id: "65c24ef3db0c2cc49cfb8d26",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });

  it("should log an error if dropping indexes fails", async () => {
    sinon
      .stub(SavedDestination.collection, "dropIndexes")
      .throws(new Error("Drop indexes error"));
    const consoleErrorSpy = sinon.spy(console, "error");

    await dropIndexes();

    expect(consoleErrorSpy.calledWithMatch(/Error dropping indexes:/)).to.be
      .true;
  });
});
