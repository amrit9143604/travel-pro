const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const app = require("../app"); // import your Express app
const Review = require("../models/review");
const Destination = require("../models/destination");
const jwt = require("jsonwebtoken");

chai.use(chaiHttp);
const expect = chai.expect;

const mockDestination = {
  name: "Gateway of India",
  city: "Mumbai",
  landmarks: ["Gateway of India", "Marine Drive"],
  state: "Maharashtra",
  description:
    "India's financial capital, known for Bollywood, diverse culture, and bustling streets",
  images: [
    "https://travel-pro.s3.amazonaws.com/goi.jpeg",
    "https://travel-pro.s3.amazonaws.com/goi_landmarks.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive.jpeg",
    "https://travel-pro.s3.amazonaws.com/marine_drive_attractions.jpeg",
    "https://travel-pro.s3.amazonaws.com/inter.jpeg",
  ],
  avgTravelExpenses: "1500 - 3000",
  attractions: ["Elephanta Caves", "Siddhivinayak Temple", "street food"],
  category: "Urban Exploration",
};

const mockReview = [
  {
    name: "amrit",
    review: "Nice place",
    rating: 5,
    destination_id: "65cfc52dbb35218b4df9fb28",
    user_id: "65cefc1177bae95d3a913868",
    createdAt: new Date("2024-02-23T12:00:00Z"),
  },
  {
    name: "amrit",
    review: "Nice place",
    rating: 5,
    destination_id: "65cfc52dbb35218b4df9fb28",
    user_id: "65cefc1177bae95d3a913868",
    createdAt: new Date("2024-02-24T12:00:00Z")
  },
];

describe("Destination Routes", () => {
  afterEach(() => {
    sinon.restore();
  });


  beforeEach(() => {
    const jwtStub = sinon.stub(jwt, "verify");
    jwtStub.callsFake((token, secret, callback) => {
      // Mocking a decoded token with role "Admin"
      const decodedToken = { role: "User" };
      callback(null, decodedToken);
    });
  });


  it("should return 400 if destination_id is missing or invalid", (done) => {
    chai
      .request(app)
      .post("/api/v1/users/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({})
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });

  it("should return 200 if destination does not exist", (done) => {
    const destintionModelMock = {
      findById: () => Promise.resolve(null),
    };

    // Override the User with the mock
    Object.assign(Destination, destintionModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fb" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 200 with destination and sorted reviews if destination exists", (done) => {
    const destintionModelMock = {
      findById: () => Promise.resolve(mockDestination),
    };

    const reviewModelMock = {
      find: () => Promise.resolve(mockReview),
    };

    // Override the User with the mock
    Object.assign(Destination, destintionModelMock);
    Object.assign(Review, reviewModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 500 if an error occurs during fetching destination", (done) => {
    const destintionModelMock = {
      findById: () => Promise.reject(new Error("Test error")),
    };
    Object.assign(Destination, destintionModelMock);

    chai
      .request(app)
      .post("/api/v1/users/get-destination")
      .set("Authorization", "Bearer mockedToken")
      .send({ destination_id: "65cefbf41befd992b72146fa" })
      .end((err, res) => {
        expect(res).to.have.status(500);
        done();
      });
  });

  it("should return 200 with all destinations if they exist", (done) => {
    const destinationModelMock = {
      find: () => Promise.resolve(mockDestination),
    };

    // Override the Destination model with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .get("/api/v1/users/get-all-destinations")
      .set("Authorization", "Bearer mockedToken")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("should return 200 with message if no destinations exist", (done) => {
    const destinationModelMock = {
      find: () => Promise.resolve(null),
    };

    // Override the Destination model with the mock
    Object.assign(Destination, destinationModelMock);

    chai
      .request(app)
      .get("/api/v1/users/get-all-destinations")
      .set("Authorization", "Bearer mockedToken")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });
});
