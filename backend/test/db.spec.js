const mongoose = require("mongoose");
const sinon = require("sinon");
const { expect } = require("chai");
const DbConnect = require("../database");

describe("DbConnect", () => {
  let connectStub;
  let consoleErrorStub;

  beforeEach(() => {
    connectStub = sinon.stub(mongoose, "connect");
    consoleErrorStub = sinon.stub(console, "error");
  });

  afterEach(() => {
    connectStub.restore();
    consoleErrorStub.restore();
  });

  it("should handle connection error", async () => {
    const error = new Error("Connection error");
    connectStub.rejects(error);

    await DbConnect();

    // Expect console.error to be called with the connection error
    expect(consoleErrorStub.calledWith("Connection error:", error)).to.be.true;
  });
});
