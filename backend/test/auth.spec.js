const chai = require("chai");
const chaiHttp = require("chai-http");
const { describe, it } = require("mocha");
const app = require("../app"); // Replace with the actual path
const User = require("../models/user"); // Assuming you have the userModel file
const sinon = require("sinon");
const CryptoJS = require("crypto-js");

chai.use(chaiHttp);
const expect = chai.expect;

const mockUser = {
  email: "hello@gmail.com",
  username: "hello",
  password: "123456",
};

describe("User Signin Routes", () => {
  it("should login a user and return a token and the user", (done) => {
    const userModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(mockUser),
      }),
    };

    // Override the User with the mock
    Object.assign(User, userModelMock);

    sinon
      .stub(CryptoJS.AES, "decrypt")
      .returns({ toString: () => "invalidPassword" });

    chai
      .request(app)
      .post("/api/v1/users/signin")
      .send({
        email: "invalidUser@gmail.com",
        password: "invalidPassword",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        expect(response.body).to.have.property("token");
        sinon.restore();
        done();
      });
  });

  it("Give 422 when the valid email or password not provided", (done) => {
    const userModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(mockUser),
      }),
    };

    // Override the User with the mock
    Object.assign(User, userModelMock);

    chai
      .request(app)
      .post("/api/v1/users/signin")
      .send({
        email: "",
        password: "",
      })
      .end((err, response) => {
        expect(response).to.have.status(422);
        done();
      });
  });

  it("Give 401 when the credentials are wrong", (done) => {
    const userModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(mockUser),
      }),
    };

    // Override the User with the mock
    Object.assign(User, userModelMock);

    chai
      .request(app)
      .post("/api/v1/users/signin")
      .send({
        email: "invalidUser@gmail.com",
        password: "invalidPassword",
      })
      .end((err, response) => {
        expect(response).to.have.status(401);
        sinon.restore();
        done();
      });
  });

  it("should return 400 with a duplicate key error message when a duplicate key error occurs during user signup", (done) => {
    const userModelMock = {};
    Object.assign(User, userModelMock);
    
    // Stub the save method of User model to reject with a duplicate key error
    sinon.stub(User.prototype, "save").rejects({
      code: 11000, // Simulate a duplicate key error
      keyValue: { email: "hello@gmail.com" } // Simulate the duplicated key
    });

    chai
      .request(app)
      .post("/api/v1/users/signup")
      .send({
        username: "amrit",
        email: "hello@gmail.com", // Simulating a duplicated email
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(400);
        done();
      });
  });

  it("Give 400 when the user does not exist", (done) => {
    const userModelMock = {
      findOne: () => ({
        exec: () => Promise.resolve(null),
      }),
    };

    // Override the User with the mock
    Object.assign(User, userModelMock);
    sinon.stub(CryptoJS.AES, "decrypt").returns({ toString: () => "1234567" });

    chai
      .request(app)
      .post("/api/v1/users/signin")
      .send({
        email: "amrit@gmail.com",
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(400);
        sinon.restore();
        done();
      });
  });

  it("Give 500 when something unusual happens", (done) => {
    const userModelMock = {
      findOne: () => ({
        exec: () => Promise.reject(),
      }),
    };

    // Override the User with the mock
    Object.assign(User, userModelMock);

    chai
      .request(app)
      .post("/api/v1/users/signin")
      .send({
        email: "amrit@gmail.com",
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        sinon.restore();
        done();
      });
  });
});

describe("Signup Routes", () => {
  afterEach(() => {
    sinon.restore();
  });
  it("should create a new user and return a token and the user", (done) => {
    const userModelMock = {};
    Object.assign(User, userModelMock);
    sinon.stub(User.prototype, "save").resolves(mockUser);
    chai
      .request(app)
      .post("/api/v1/users/signup")
      .send({
        username: "amrit",
        email: "amrit@gmail.com",
        password: "123456",
      })
      .end((err, response) => {
        expect(response).to.have.status(200);
        expect(response.body).to.have.property("token");
        done();
      });
  });

  it("Give 500 when something unusual happens", (done) => {
    const userModelMock = {};
    Object.assign(User, userModelMock);

    chai
      .request(app)
      .post("/api/v1/users/signup")
      .send({
        username: "",
        email: "",
        password: ""
      })
      .end((err, response) => {
        expect(response).to.have.status(500);
        done();
      });
  });
});


describe("Signout Routes", () => {
  afterEach(() => {
    sinon.restore();
  });
  it("should signout the user", (done) => {
    const userModelMock = {};
    Object.assign(User, userModelMock);
    chai
      .request(app)
      .get("/api/v1/users/signout")
      .end((err, response) => {
        expect(response).to.have.status(200);
        done();
      });
  });
});
