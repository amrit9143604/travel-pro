// app.js
const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const DbConnect = require("./database");
// const http = require('http');
const app = express();
// const server = http.createServer(app);
// const io = require("socket.io")(server, {
// 	cors: {
// 		origin: "*",
// 		methods: ["GET", "POST"],
// 	},
// });
const axios = require("axios");

const PORT = process.env.PORT || 5000;
const cors = require("cors");

dotenv.config();

DbConnect();
app.use(bodyParser.json());
app.use(cors());

// io.on('connection', (socket) => {
//   console.log('a user connected');

//   socket.on('chat message', (msg) => {
//     console.log('message: ', msg);
//     // Broadcast the message to all connected clients
//     io.emit('chat message', msg);
//   });

//   socket.on('disconnect', () => {
//     console.log('user disconnected');
//   });
// });

// const corsOptions = {
//   origin: ['http://localhost:3000', 'https://travel-pro-eight.vercel.app'],
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// };

app.get("/", (req, res) => {
  res.status(200).json({ message: "Hello from the servers of travel pro" });
});

app.use("/api/v1", require("./routes/users"));
app.use("/api/v1", require("./routes/admin/admins"));
app.use("/api/v1", require("./routes/admin/destinations"));
app.use("/api/v1", require("./routes/admin/itineraries"));
app.use("/api/v1", require("./routes/admin/uploads"));
app.use("/api/v1", require("./routes/saveStore"));
app.use("/api/v1", require("./routes/destinations"));
app.use("/api/v1", require("./routes/reviews"));
app.use("/api/v1", require("./routes/itinerary"));
app.use("/api/v1", require("./routes/search"));

// Initialising/Starting the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

module.exports = app;
