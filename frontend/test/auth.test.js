import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import axios from "axios";
import Login from "../pages/users/login/index";
import Signup from "../pages/users/signup/index";
import { useRouter } from "next/router";

// Mock the useRouter hook
jest.mock("next/router", () => ({
  useRouter: jest.fn(),
}));

jest.mock("axios");

describe("Login component", () => { 
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("renders login form correctly", () => {
    useRouter.mockReturnValue({
      basePath: "",
      push: jest.fn(),
      replace: jest.fn(),
      pathname: "/",
      query: {},
      asPath: "/",
    });
    render(<Login />);
  });
});

describe("Signup component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("renders signup form correctly", () => {
    useRouter.mockReturnValue({
      basePath: "",
      push: jest.fn(),
      replace: jest.fn(),
      pathname: "/",
      query: {},
      asPath: "/",
    });
    render(<Signup />);
  });
});
