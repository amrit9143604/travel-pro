/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
}

module.exports = nextConfig

module.exports = {
  images: {
    domains: ['travel-pro.s3.ap-south-1.amazonaws.com', "travel-pro.s3.amazonaws.com", "s3-us-west-2.amazonaws.com", "cdn.weatherapi.com"],
  },
}
