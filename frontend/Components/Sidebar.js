import { useEffect, useState } from "react";
import Link from "next/link";
import styles from "../styles/sidebar.module.css";
import { navData } from "./navData";
import Image from "next/image";
import destination from "../assets/destination.png";
import { AiOutlineLogout } from "react-icons/ai";
import { RiFeedbackLine } from "react-icons/ri";
import Cookies from "js-cookie";
import { useRouter } from "next/router";
import axios from "axios";
import { BASE_URL, API_URL } from "../constants";
import cookie from "js-cookie";

export default function SideBar({ setLoading }) {
  const router = useRouter();
  const { pathname } = router;
  const [token, setToken] = useState("");

  useEffect(() =>{
    const data = Cookies.get("token");
    setToken(data);
  })

  const logout = () => {
    Cookies.remove("token");
    Cookies.remove("username");
    Cookies.remove("user_id");
    router.push("/users/login");
    setLoading(true);
  };

  const getData = (item) => {
    if (
      item.link == `/users/${pathname.substring(pathname.lastIndexOf("/") + 1)}`
    ) {
      setLoading(false);
    } else {
      setLoading(true);
      router.push(`${item.link}`);
    }
  };

  return (
    <div className={styles.sidenav}>
      <div className={styles.sidenav_logo}>
        <Image
          src={destination}
          width={120}
          height={120}
          alt="destination_logo"
        />
      </div>

      {navData.map((item) => {
        return (
          <div
            key={item.id}
            className={styles.sideitem}
            href={item.link}
            onClick={() => getData(item)}
          >
            {item.icon}
            <span className={styles.linkText}>{item.text}</span>
          </div>
        );
      })}

      <div className={styles.sideitem}>
        <RiFeedbackLine />
        <form
        className={styles.formExp}
          action="http://13.232.14.69/login"
          method="post"
        >
          <button className={styles.linkButton} value={token} name="token">
            See Experiences
          </button>
        </form>
      </div>
      <div className={styles.sideitem} onClick={() => logout()}>
        <AiOutlineLogout />
        <span className={styles.linkText}>Logout</span>
      </div>
    </div>
  );
}
