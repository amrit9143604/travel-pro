import { Rating } from "react-simple-star-rating";

export default function reviewModal({ review, setReview, handleRating, handleReview }) {
    const onPointerEnter = () => console.log("Enter");
    const onPointerLeave = () => console.log("Leave");
    const onPointerMove = (value, index) => console.log(value, index);
  
  return (
    <div className="add_review_div">
    <p>Add Review</p>
    <label>Review</label>
    <input
      type="name"
      value={review}
      onChange={(e) => setReview(e.target.value)}
    />

    <label>Rating</label>
    <Rating
      onClick={handleRating}
      onPointerEnter={onPointerEnter}
      onPointerLeave={onPointerLeave}
      onPointerMove={onPointerMove}
      /* Available Props */
    />

    <button onClick={() => handleReview()}>Add Review</button>
  </div>
  );
}
