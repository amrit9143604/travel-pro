import { useState, useRef, useEffect } from "react";
import { Configuration, OpenAIApi } from "openai";
import chatbot from "../assets/chatbot.png";
import Image from "next/image";
import man from "../assets/man.png";
import bot from "../assets/bot.png";
import { RxCross2 } from "react-icons/rx";

const configuration = new Configuration({
  apiKey: process.env.NEXT_PUBLIC_OPENAI_API_URL,
});
const openai = new OpenAIApi(configuration);

export default function ChatBot() {
  const [isOpen, setIsOpen] = useState(false);
  const [isBotOpen, setIsBotOpen] = useState(true);
  const [message, setMessage] = useState("");
  const [chats, setChats] = useState([]);
  const [isTyping, setIsTyping] = useState(false);
  const chatMessagesRef = useRef("chatbot-messages");

  useEffect(() => {
    scrollToBottom();
  }, [chats]);

  const toggleChat = () => {
    setIsOpen(!isOpen);
    setIsBotOpen(!isBotOpen);
  };

  const scrollToBottom = () => {
    if (chatMessagesRef.current) {
      chatMessagesRef.current.scrollTop = chatMessagesRef.current.scrollHeight;
    }
  };

  let msgs = chats; // Copy the chats array
  const chat = async (e, message) => {
    try {
      e.preventDefault();
      scrollToBottom();

      if (!message) return;
      setIsTyping(true);

      msgs.push({ role: "user", content: message });
      setChats(msgs);

      setMessage("");
      scrollToBottom();

      await openai
        .createChatCompletion({
          model: "gpt-3.5-turbo",
          messages: [
            {
              role: "system",
              content: "You are a EbereGPT. You can help with today",
            },
            ...chats,
          ],
          max_tokens: process.env.INTEGER_VARIABLE,
        })
        .then((res) => {
          scrollToBottom();
          const responseMsg = res.data.choices[0].message;
          msgs.push(responseMsg);
          setChats(msgs);
          setIsTyping(false);
        })
        .catch((err) => {
          if (err.response.status != 200) {
            // const responseMsg = err.response.data.error.message;
            // console.log(responseMsg);
            setIsTyping(false);
            msgs.push("Server is having a high load, please try again later!!");
            // setChats(msgs);
            setChats(msgs);
            console.log(chats);
          }
        });
    } catch (error) {
      setIsTyping(false);
      msgs.push("Server is having a high load, please try again later!!");
      setChats(msgs);
      console.log(error);
    }
  };

  return (
    <div className="chatbot-container">
      <div className={`chatbot ${isOpen ? "open" : ""}`}>
        <form className="chatbot-input" onSubmit={(e) => chat(e, message)}>
          <input
            type="text"
            name="message"
            value={message}
            placeholder="Type a message here and hit Enter..."
            onChange={(e) => {
              setMessage(e.target.value);
              scrollToBottom(); // Scroll to bottom when typing
            }}
          />
          <button type="submit">Send</button>
        </form>
        <div
          className="chatbot-messages"
          id="chat-messages"
          ref={chatMessagesRef}
        >
          <RxCross2
            className="close-icon"
            size={30}
            onClick={toggleChat}
          />
          {chats.map((chat, index) => (
            <div
              key={index}
              className={chat.role === "user" ? "user-msg" : "bot-msg"}
            >
              {chat.role != "user" ? (
                <div className="bot-chat-content">
                  <Image src={bot} height={45} width={45} alt="bot" />
                  <div className="chat-content-bot">
                    {chat.content
                      ? chat.content
                      : "Server is having high load, please try again later or just reload the page!!"}
                  </div>
                </div>
              ) : (
                <div className="user-chat-content">
                  <div className="chat-content">{chat.content}</div>
                  <Image src={man} height={45} width={45} alt="user" />
                </div>
              )}
            </div>
          ))}
          {isTyping && (
            <div className="bot-chat-content">
              <Image src={bot} height={45} width={45} alt="bot" />
              <div className="chat-content-bot">Typing...</div>
            </div>
          )}
        </div>
      </div>

      {isBotOpen && (
        <Image
          className="chatbot-button"
          onClick={toggleChat}
          src={chatbot}
          height={70}
          width={70}
          alt="chatbot-button"
        />
      )}
    </div>
  );
}
