import { ToastContainer, toast } from "react-toastify";


export default function Toast(){
    return (
        <ToastContainer
        position="top-right"
        autoClose={1000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        draggable
        theme="dark"
      />
    )
}