import { IoAddCircleOutline } from "react-icons/io5";
import { GrSubtractCircle } from "react-icons/gr";
import moment from "moment";

export default function itineraryUpdateModal({
  updateFormData,
  updateHandleChange,
  addRow,
  rows,
  setRows,
  removeRow,
  updateDisplayValues,
  updatedDisplayedValues,
  displayedValues,
  updateItinerary,
  removeDisplayedRow,
}) {
  return (
    <div>
      <div className="add_itinerary_modal_div">
        <div className="add_itinerary_div">
          <p>Update Itinerary</p>
        </div>
        <div>
          <div>
            <label>Plan Name / Source</label>
            <input
              value={updateFormData.planName}
              type="text"
              name="planName"
              onChange={updateHandleChange}
            />
          </div>

          <div>
            <label>Travel Mode</label>
            <input
              value={updateFormData.travelMode}
              type="text"
              name="travelMode"
              onChange={updateHandleChange}
            />
          </div>
          <div>
            <label>Start Date</label>
            <input
              value={updateFormData.travelStartDate}
              type="date"
              name="travelStartDate"
              onChange={updateHandleChange}
            />
          </div>
        </div>

        <div>
          <div>
            <label>Destination</label>
            <input
              value={updateFormData.destination}
              type="text"
              name="destination"
              onChange={updateHandleChange}
            />
          </div>

          <div>
            <label>Cost</label>
            <input
              value={updateFormData.estimatedCost}
              type="text"
              name="estimatedCost"
              onChange={updateHandleChange}
            />
          </div>
          <div>
            <label>End Date</label>
            <input
              value={updateFormData.travelEndDate}
              type="date"
              name="travelEndDate"
              onChange={updateHandleChange}
            />
          </div>
        </div>

        <div className="add_row_modal_div">
          <IoAddCircleOutline
            style={{ cursor: "pointer" }}
            size={35}
            onClick={addRow}
          />
        </div>
        {rows.map((row) => (
          <div key={row.id} className="add_input_modal_div">
            <div>
              <label>Location</label>
              <input
                type="text"
                value={row.location}
                onChange={(e) => {
                  const updatedRows = [...rows];
                  updatedRows[row.id - 1].location = e.target.value;
                  setRows(updatedRows);
                }}
              />
            </div>
            <div>
              <label>Date</label>
              <input
                type="date"
                value={row.date}
                onChange={(e) => {
                  const updatedRows = [...rows];
                  updatedRows[row.id - 1].date = e.target.value;
                  setRows(updatedRows);
                }}
              />
            </div>

            <GrSubtractCircle
              style={{ marginTop: "2rem", cursor: "pointer" }}
              size={25}
              onClick={() => removeRow(row.id)}
            />

            <p
              className="add_location_date_div"
              onClick={() => updateDisplayValues(row.id)}
            >
              Add
            </p>
          </div>
        ))}
        {displayedValues.map((value, index) => (
          <div key={index} className="displayed_values_div">
            <p>
              {moment(value.date).utc().format("DD-MM-YYYY")}{" "}
              ------------------------------------------------{" "}
              {value.dayDetails}
            </p>

            <GrSubtractCircle
              style={{ margin: "-1rem 1rem 0rem 1rem", cursor: "pointer" }}
              size={25}
              onClick={() => removeDisplayedRow(index)}
            />
          </div>
        ))}

        {updatedDisplayedValues.map((value, index) => (
           <p className="displayed_values_div" key={index}>{value}</p>
        ))}
      </div>

      <div className="create_itinerary_main_div">
        <div
          onClick={() => updateItinerary()}
          className="create_itinerary_button_div"
        >
          Update Itinerary
        </div>
      </div>
    </div>
  );
}
