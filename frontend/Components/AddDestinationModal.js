export default function AddDestinationModal({
  formData,
  handleChange,
  handleSubmit,
  handleImageChange,
  uploadImages,
  images,
}) {
  return (
    <div>
      <div className="add_itinerary_modal_div">
        <div className="add_itinerary_div">
          <p>Add Destination</p>
        </div>
        <div>
          <div>
            <label>Name</label>
            <input
              value={formData.name}
              type="text"
              name="name"
              onChange={handleChange}
            />
          </div>

          <div>
            <label>City</label>
            <input
              value={formData.city}
              type="text"
              name="city"
              onChange={handleChange}
            />
          </div>
          <div>
            <label>State</label>
            <input
              value={formData.state}
              type="text"
              name="state"
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Landmarks</label>
            <input
              value={formData.landmarks}
              type="text"
              name="landmarks"
              onChange={handleChange}
            />
          </div>
        </div>

        <div>
          <div>
            <label>Description</label>
            <textarea
              value={formData.description}
              type="text"
              name="description"
              rows={2}
              cols={22}
              onChange={handleChange}
            />
          </div>

          <div>
            <label>Average Travel Expenses</label>
            <input
              value={formData.avgTravelExpenses}
              type="text"
              name="avgTravelExpenses"
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Category</label>
            <input
              value={formData.category}
              type="text"
              name="category"
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Attractions</label>
            <input
              value={formData.attractions}
              type="text"
              name="attractions"
              onChange={handleChange}
            />
          </div>
        </div>
      </div>
      <div className="upload_images">
        <label>Images</label>
        <input
          type="file"
          name="images"
          onChange={handleImageChange}
          accept="image/*"
          multiple // Allow multiple file selection
        />
        <div className="show_images_add_div">
          {images.map((image, index) => (
            <p key={index}>{image.name}</p>
          ))}
        </div>
      </div>

      <div className="upload_images_main_div">
        <div
          onClick={() => uploadImages()}
          className="create_itinerary_button_div"
        >
          Upload images
        </div>
      </div>

      <div className="create_itinerary_main_div">
        <div
          onClick={() => handleSubmit()}
          className="create_itinerary_button_div"
        >
          Create Destination
        </div>
      </div>
    </div>
  );
}
