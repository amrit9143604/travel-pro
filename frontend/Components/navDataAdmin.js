import { FaMapMarkedAlt } from "react-icons/fa";
import { FaMapMarkerAlt } from "react-icons/fa";
import { GiWavyItinerary } from "react-icons/gi";
import { FaBookmark } from "react-icons/fa";
import { BsFillBagCheckFill } from 'react-icons/bs';
import { RiFeedbackLine } from "react-icons/ri";
import { MdOutlineExplore } from "react-icons/md";


export const navDataAdmin =  [
    {
        id: 0,
        icon: <FaMapMarkedAlt/>,
        text: "Add Destination",
        link: '/admin/dashboard'
    },



    {
        id: 3,
        icon: <GiWavyItinerary/>,
        text: "Add Itinerary",
        link: `/admin/itinerary`
    },

   
]