export default function DestinationUpdateModal({
  updateFormData,
  updateHandleChange,
  updateDestination,
}) {
  return (
    <div>
      <div className="add_itinerary_modal_div">
        <div className="add_itinerary_div">
          <p>Update Destination</p>
        </div>
        <div>
          <div>
            <label>Name</label>
            <input
              value={updateFormData.name}
              type="text"
              name="name"
              onChange={updateHandleChange}
            />
          </div>

          <div>
            <label>City</label>
            <input
              value={updateFormData.city}
              type="text"
              name="city"
              onChange={updateHandleChange}
            />
          </div>
          <div>
            <label>State</label>
            <input
              value={updateFormData.state}
              type="text"
              name="state"
              onChange={updateHandleChange}
            />
          </div>
          <div>
            <label>Landmarks</label>
            <input
              value={updateFormData.landmarks}
              type="text"
              name="landmarks"
              onChange={updateHandleChange}
            />
          </div>
        </div>

        <div>
          <div>
            <label>Description</label>
            <input
              value={updateFormData.description}
              type="text"
              name="description"
              onChange={updateHandleChange}
            />
          </div>

          <div>
            <label>Average Travel Expenses</label>
            <input
              value={updateFormData.avgTravelExpenses}
              type="text"
              name="avgTravelExpenses"
              onChange={updateHandleChange}
            />
          </div>
          <div>
            <label>Category</label>
            <input
              value={updateFormData.category}
              type="text"
              name="category"
              onChange={updateHandleChange}
            />
          </div>

          <div>
            <label>Attractions</label>
            <input
              value={updateFormData.attractions}
              type="text"
              name="attractions"
              onChange={updateHandleChange}
            />
          </div>
        </div>

        {updateFormData.images.map((im) => {
          return (
            <div className="show_images_update_div">
              <a href={im} target="_blank">{im}</a>
            </div>
          );
        })}
      </div>

      <div className="create_itinerary_main_div">
        <div
          onClick={() => updateDestination()}
          className="create_itinerary_button_div"
        >
          Update Destination
        </div>
      </div>
    </div>
  );
}
