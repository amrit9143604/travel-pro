import loading from '../assets/loading.gif'
import Image from 'next/image';

const Loading = () =>{
    return(
        <div className='loading'>
            <Image src={loading} alt='loading' />
        </div>
    )
}

export default Loading;