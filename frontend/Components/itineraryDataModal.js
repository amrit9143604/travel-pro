import moment from "moment";

export default function itineraryDataModal({ modalData }) {
  return (
    <div className="modal_data_details_div">
      <p>Name / Source: {modalData?.planName}</p>
      <p>Destination: {modalData?.destination}</p>
      <p>
        From: {moment(modalData?.travelStartDate).utc().format("DD-MM-YYYY")}
      </p>
      <p>To: {moment(modalData?.travelEndDate).utc().format("DD-MM-YYYY")}</p>
      <p>Travel Mode: {modalData?.travelMode}</p>
      <p>Cost: ₹{modalData?.estimatedCost}</p>
      <div>
        <p>Details</p>
        <table style={{ textAlign: "left" }}>
          <tr>
            <th>Date</th>
            <th>Place</th>
          </tr>
          {modalData?.details.map((md, index) => {
            return (
              <div key={index}>
                <tr>
                  <td>{moment(md.date).utc().format("DD-MM-YYYY")}</td>
                  <td>{md.dayDetails}</td>
                </tr>
              </div>
            );
          })}
        </table>
      </div>
    </div>
  );
}
