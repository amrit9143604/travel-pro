import { IoAddCircleOutline } from "react-icons/io5";
import { GrSubtractCircle } from "react-icons/gr";

export default function itineraryModal({
  formData,
  handleChange,
  addRow,
  rows,
  setRows,
  removeRow,
  displayValues,
  displayedValues,
  handleSubmit,
}) {
  return (
    <div>
      <div className="add_itinerary_modal_div">
        <div className="add_itinerary_div">
          <p>Add Itinerary</p>
        </div>
        <div>
          <div>
            <label>Plan Name / Source</label>
            <input
              value={formData.planName}
              type="text"
              name="planName"
              onChange={handleChange}
            />
          </div>

          <div>
            <label>Travel Mode</label>
            <input
              value={formData.travelMode}
              type="text"
              name="travelMode"
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Start Date</label>
            <input
              value={formData.travelStartDate}
              type="date"
              name="travelStartDate"
              onChange={handleChange}
            />
          </div>
        </div>

        <div>
          <div>
            <label>Destination</label>
            <input
              value={formData.destination}
              type="text"
              name="destination"
              onChange={handleChange}
            />
          </div>

          <div>
            <label>Cost</label>
            <input
              value={formData.estimatedCost}
              type="text"
              name="estimatedCost"
              onChange={handleChange}
            />
          </div>
          <div>
            <label>End Date</label>
            <input
              value={formData.travelEndDate}
              type="date"
              name="travelEndDate"
              onChange={handleChange}
            />
          </div>
        </div>

        <div className="add_row_modal_div">
          <IoAddCircleOutline
            style={{ cursor: "pointer" }}
            size={35}
            onClick={addRow}
          />
        </div>
        {rows.map((row) => (
          <div key={row.id} className="add_input_modal_div">
            <div>
              <label>Location</label>
              <input
                type="text"
                value={row.location}
                onChange={(e) => {
                  const updatedRows = [...rows];
                  updatedRows[row.id - 1].location = e.target.value;
                  setRows(updatedRows);
                }}
              />
            </div>
            <div>
              <label>Date</label>
              <input
                type="date"
                value={row.date}
                onChange={(e) => {
                  const updatedRows = [...rows];
                  updatedRows[row.id - 1].date = e.target.value;
                  setRows(updatedRows);
                }}
              />
            </div>

            <GrSubtractCircle
              style={{ marginTop: "2rem", cursor: "pointer" }}
              size={25}
              onClick={() => removeRow(row.id)}
            />

            <p
              className="add_location_date_div"
              onClick={() => displayValues(row.id)}
            >
              Add
            </p>
          </div>
        ))}
        {displayedValues.map((value, index) => (
          <p className="displayed_values_div" key={index}>{value}</p>
        ))}
      </div>

      <div className="create_itinerary_main_div">
        <div
          onClick={() => handleSubmit()}
          className="create_itinerary_button_div"
        >
          Create Itinerary
        </div>
      </div>
    </div>
  );
}
