import { useState } from "react";
import Link from "next/link";
import styles from "../styles/sidebar.module.css";
import { navDataAdmin } from "./navDataAdmin";
import Image from "next/image";
import destination from "../assets/destination.png";
import { AiOutlineLogout } from "react-icons/ai";
import { BsFillBagCheckFill } from "react-icons/bs";
import Cookies from "js-cookie";
import { useRouter } from "next/router";

export default function SideBarAdmin({ setLoading }) {
  const router = useRouter();
  const { pathname } = router;

  const logout = () => {
    Cookies.remove("token");
    Cookies.remove("username");
    Cookies.remove("user_id");
    router.push("/admin/login");
    setLoading(true);
  };

  const getData = (item) => {
    if (item.link == `/admin/${pathname.substring(pathname.lastIndexOf("/") + 1)}`) {
      setLoading(false);
    } else {
      setLoading(true);
      router.push(`${item.link}`)
    }
  };

  return (
    <div className={styles.sidenav}>
      <div className={styles.sidenav_logo}>
        <Image
          src={destination}
          width={120}
          height={120}
          alt="destination_logo"
        />
      </div>

      {navDataAdmin.map((item) => {
        return (
          <div
            key={item.id}
            className={styles.sideitem}
            href={item.link}
            onClick={() => getData(item)}
          >
            {item.icon}
            <span className={styles.linkText}>{item.text}</span>
          </div>
        );
      })}

      <div className={styles.sideitem} onClick={() => logout()}>
        <AiOutlineLogout />
        <span className={styles.linkText}>Logout</span>
      </div>
    </div>
  );
}
