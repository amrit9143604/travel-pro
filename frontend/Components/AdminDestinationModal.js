import { Rating } from "react-simple-star-rating";
import { FaUserCircle } from "react-icons/fa";
import { Modal } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";

export default function AdminDestinationModal({ modalData, name }) {
  return (
    <div className="destination_modal_div">
      <div className="destination_modal">
        <div>
          <p>{modalData?.name}</p>
        </div>
        <div className="modal_weather">
          <p>{modalData?.currentWeather}</p>
          <Image
            className="weather_icon"
            src={`http:${modalData?.weatherIcon}`}
            width={50}
            height={50}
            alt="destination"
          />
          <p>{modalData?.temperature}°C</p>
        </div>
      </div>
      <hr></hr>

      <div className="modal_description_div">
        <p> {modalData?.description}</p>
        <div>
          {modalData?.landmarks.map((lm) => {
            return <p key={lm}>{lm},</p>;
          })}
        </div>
        <div className="modal_image_div">
          {modalData?.images.map((di) => {
            return (
              <Image
                key={di}
                className="modal_image"
                src={di}
                width={300}
                height={230}
            alt="destination"
              />
            );
          })}
        </div>
      </div>

      <div>
        <div className="state_category_div">
          <p>{modalData?.state}</p>
          <p>{modalData?.category}</p>
        </div>
        <p className="state_category_div">{`₹${modalData?.avgTravelExpenses}/night`}</p>
      </div>

      <div className="attractions_directions_div">
        <div className="attractions_div">
          {modalData?.attractions.map((at) => {
            return <p key={at}>{at},</p>;
          })}
        </div>

        <div className="get_directions_button">
          <Link
            href={`https://www.google.com/maps/place/${modalData?.latitude},${modalData?.longitude}/`}
            target="_blank"
            prefetch={false}
          >
            <button>Get Directions</button>
          </Link>
        </div>
      </div>

     
    </div>
  );
}
