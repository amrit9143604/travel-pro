import { FaMapMarkedAlt } from "react-icons/fa";
import { FaMapMarkerAlt } from "react-icons/fa";
import { GiWavyItinerary } from "react-icons/gi";
import { FaBookmark } from "react-icons/fa";
import { BsFillBagCheckFill } from 'react-icons/bs';
import { MdOutlineExplore } from "react-icons/md";


export const navData =  [
    {
        id: 0,
        icon: <FaMapMarkedAlt/>,
        text: "Dashboard",
        link: '/users/dashboard'
    },
    {
        id: 1,
        icon: <FaMapMarkerAlt/>,
        text: "Explore Maps",
        link: "/users/maps"
    },

    {
        id: 2,
        icon: <MdOutlineExplore/>,
        text: "Explore Itineraries",
        link: `/users/explore-itineraries`
    },


    {
        id: 3,
        icon: <GiWavyItinerary/>,
        text: "Itinerary",
        link: `/users/itinerary`
    },

    {
        id: 4,
        icon: <FaBookmark/>,
        text: "Saved Destinations",
        link: `/users/saved-destinations`
    },
    {
        id: 5,
        icon: <BsFillBagCheckFill/>,
        text: "Saved Itineraries",
        link: `/users/saved-itineraries`
    },
  ]