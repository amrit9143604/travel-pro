import Head from "next/head";
import Image from "next/image";
import travelBackground from "../assets/image.jpg";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();
  return (
    <div>
      <Head>
        <title>Travel Pro</title>
        <meta
          name="description"
          content="Explore the world with your travel buddy"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="travel_image_text">
        <div className="travel_image">
          <Image
            className="travel_background_image"
            src={travelBackground}
            alt="travel_background_image"
            fill={true}
          />
          <div className="travel_name_div">
            <p>Travel Pro</p>
          </div>

          <div className="travel_slogan_button_div">
            <div className="travel_slogan_div">
              <p>Explore Beyond, Escape the</p>
              <p> Everyday Norms, Embrace and </p>
              <p>Celebrate the Unconventional!</p>
            </div>

            <div className="travel_button_div">
              <p>Travel with us</p>
              <div>
                <button
                  onClick={() => router.push("/users/login")}
                  className="login_button"
                >
                  Login
                </button>
                <button
                  onClick={() => router.push("/users/signup")}
                  className="signup_button"
                >
                  Signup
                </button>
              </div>
              <div className="admin_login_button">
                <button
                  onClick={() => router.push("/admin/login")}
                  className="login_button"
                >
                  Admin Login
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
