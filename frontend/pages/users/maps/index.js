import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { BASE_URL, API_URL } from "../../../constants";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";
import Loading from "../../../Components/Loading";
import Sidebar from "../../../Components/Sidebar";
import { Modal } from "react-bootstrap";
import { useEffect } from "react";
import DestinationModal from "../../../Components/destinationModal";
import ReviewModal from "../../../Components/reviewModal";

export default function Login({ allDestinations }) {
  const router = useRouter();
  const [token, setToken] = useState("");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [loading, setLoading] = useState(false);
  const [modalData, setModalData] = useState(null);
  const [reviewData, setReviewData] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [reviewModal, setReviewModal] = useState(false);
  const [destinationId, setDestinationId] = useState("");
  const [reviews, setReviews] = useState([]);
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");

  useEffect(() => {
    const tokenFromCookie = Cookies.get("token");
    const idFromCookie = Cookies.get("user_id");
    const nameFromCookie = Cookies.get("username");
    setToken(tokenFromCookie || "");
    setId(idFromCookie || "");
    setName(nameFromCookie || "");
  }, []);

  const handleRating = (rate) => {
    setRating(rate);
    console.log(rate);
  };

  const handleDestination = async (destination_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/get-destination`,
        {
          destination_id: destination_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      setLoading(false);
      setDestinationId(destination_id);
      setModalData(response.data.data.destination);
      setReviewData(response.data.data);
      setShowModal(true);
      setReviews(response.data.data.reviews);
    } catch (error) {
      setLoading(false);
      console.error("Error fetching destination data:", error);
    }
  };

  const handleReview = async () => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/add-review`,
        {
          name: name,
          review: review,
          rating: rating,
          destination_id: destinationId,
          user_id: id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      if (response.status == 200) {
        setLoading(false);
        setReviews([...reviews, response.data.review]);
        setReviewModal(false);
        toast.success("Review Added");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error fetching destination data:", error);
      toast.error("Error getting the destination, please try again later!!");
    }
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const closeReviewModal = () => {
    setReviewModal(false);
  };

  return (
    <div>
      <Head>
        <title>Explore Maps | Travel Pro</title>
        <meta name="description" content="Explore maps in travel pro" />
        <link rel="icon" href="/favicon.ico" />

        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
          crossorigin="anonymous"
        />
        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
          crossorigin="anonymous"
          async
        ></script>
      </Head>

      <div>
        <div>
          <div>
            <Sidebar setLoading={setLoading} />
          </div>

          <div className="explore_maps_main_div">
            <div className="explore_maps_div">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d837.643595323997!2d78.37508425264076!3d17.44769426081145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb91449d140b2b%3A0xfc3a4f97e4236622!2sDarwinbox!5e0!3m2!1sen!2sin!4v1707469751847!5m2!1sen!2sin"
                width="1570"
                height="600"
                allowfullscreen
                alt="destination"
              ></iframe>
            </div>
            <div className="maps_destinations_div">
              {allDestinations &&
                allDestinations?.map((ad, index) => {
                  return (
                    <div className="destination_image_maps_div" key={ad._id}>
                      <div>
                        <Image
                          className="destination_image"
                          src={ad.images[0]}
                          width={300}
                          height={230}
                          onClick={() => handleDestination(ad._id)}
                        />
                      </div>

                      <div className="name_travel_favourite_div">
                        <div>
                          <p>{ad.name}</p>
                          <p className="avgTravel_div">
                            ₹{ad.avgTravelExpenses}/night
                          </p>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>

          <Modal
            size="xl"
            show={showModal}
            onHide={closeModal}
            aria-labelledby="example-custom-modal-styling-title"
            centered
          >
            <DestinationModal
              modalData={modalData}
              reviews={reviews}
              setReviewModal={setReviewModal}
              name={name}
            />
          </Modal>

          <Modal
            size="sm"
            show={reviewModal}
            onHide={closeReviewModal}
            aria-labelledby="example-custom-modal-styling-title"
            centered
          >
            <ReviewModal
              review={review}
              setReview={setReview}
              handleRating={handleRating}
              handleReview={handleReview}
            />
          </Modal>
          <Toast />
        </div>
      </div>
      {loading && <Loading />}
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const res = await axios.get(
      `${BASE_URL}${API_URL}/users/get-all-destinations`,
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );

    const resSaved = await axios.post(
      `${BASE_URL}${API_URL}/users/get-saved-destinations`,
      {
        user_id: context.req.cookies.user_id,
      },
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );
    const resDestination = res.data.data;
    const resSavedDestination = resSaved.data.destinations;
    console.log(resSavedDestination);
    return {
      props: {
        allDestinations: resDestination,
        savedDestinations: resSavedDestination,
      },
    };
  } catch (err) {
    console.log(err.response.status);
    if (err.response.status == 500) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else if (err.response.status == 401 || 403) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else {
      return {
        props: {
          error: [
            {
              error: "retry",
            },
          ],
        },
      };
    }
  }
}
