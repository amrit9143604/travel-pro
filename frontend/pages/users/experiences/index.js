import Head from "next/head";
import Image from "next/image";
import compass from "../../../assets/compass.gif";
import { useRouter } from "next/router";
import { useState } from "react";
import axios from "axios";
import cookie from "js-cookie";
import { BASE_URL, API_URL } from "../../../constants";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";
import Loading from "../../../Components/Loading";

export default function Experiences() {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    try {
      setLoading(true)
      if (!email || !password) {
        console.log("email and password not provided");
        toast.error("Email or password not provided!!")
        setLoading(false)
        return;
      } else {
        const response = await axios.post(
          `${BASE_URL}${API_URL}/users/signin`,
          {
            email,
            password,
          }
        );
        console.log(response);
        if (response.status == 200) {
          cookie.set("token", response.data.token);
          cookie.set("user_id", response.data.user._id);
          cookie.set("username", response.data.user.username);
          toast.success("Login Successfull!")
          router.push("/users/dashboard");
        }
      }
    } catch (error) {
      setLoading(false)
      console.error("Error:", error);
      toast.error("Error signing in the user, please try again later!!")

    }
  };

  return (
    <div>
      <Head>
        <title>Login | Travel Pro</title>
        <meta name="description" content="Login to travel pro" />
        <link rel="icon" href="/favicon.ico" />
       
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
          crossorigin="anonymous"
        />
        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
          crossorigin="anonymous"
          async
        ></script>
      </Head>

      <div>
        
      </div>
    </div>
  );
}
