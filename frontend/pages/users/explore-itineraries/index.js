import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import axios from "axios";
import Sidebar from "../../../Components/Sidebar";
import { BASE_URL, API_URL } from "../../../constants";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import Modal from "react-bootstrap/Modal";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import Loading from "../../../Components/Loading";
import ItineraryDataModal from "../../../Components/itineraryDataModal";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";

export default function Itinerary({ allItineraries, savedItineraries }) {
  const router = useRouter();

  const [token, setToken] = useState("");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [itineraryId, setItineraryId] = useState("");
  const [heartStates, setHeartStates] = useState([]);
  const [modalData, setModalData] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showModalData, setShowModalData] = useState(false);
  const [loading, setLoading] = useState(false);
  const [itineraries, setItineraries] = useState(allItineraries);
  const [searchCriteria, setSearchCriteria] = useState({
    startDate: "",
    endDate: "",
    source: "",
    destination: "",
    cost: "",
  });

  const [saved, setSaved] = useState(savedItineraries);

  useEffect(() => {
    const tokenFromCookie = Cookies.get("token");
    const idFromCookie = Cookies.get("user_id");
    const nameFromCookie = Cookies.get("username");
    setToken(tokenFromCookie || "");
    setId(idFromCookie || "");
    setName(nameFromCookie || "");
    fetchSavedItineraries();
  }, []);

  const fetchSavedItineraries = async () => {
    try {
      setLoading(true);
      const savedItinerariesIds = savedItineraries.map(
        (itinerary) => itinerary._id
      );
      const initialHeartStates = itineraries.map((itinerary) =>
        savedItinerariesIds.includes(itinerary._id)
      );
      setLoading(false);
      setHeartStates(initialHeartStates);
    } catch (error) {
      setLoading(false);
      console.error("Error fetching saved itineraries:", error);
    }
  };

  const handleButtonClick = async (index, itinerary_id) => {
    try {
      setLoading(true);
      setHeartStates((prevHeartStates) => {
        const newHeartStates = [...prevHeartStates];
        newHeartStates[index] = !newHeartStates[index];
        return newHeartStates;
      });
      const data = await axios.post(
        `${BASE_URL}${API_URL}/users/save-itinerary`,
        {
          itinerary_id: itinerary_id,
          user_id: id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      if (data.status == 200) {
        setLoading(false);
        if (data.data.message == "Added to your saved itinerary list") {
          setSaved((prevSaved) => [...prevSaved, { _id: itinerary_id }]);
          toast.success("Added to your saved itinerary list");
        } else {
          setSaved((prevSaved) =>
            prevSaved.filter((itinerary) => itinerary._id !== itinerary_id)
          );
          toast.success("Removed from your saved itinerary list");
        }
      }
    } catch (error) {
      setLoading(false);
      toast.error('Error in saving the itinerary list')
      console.error("Error fetching saved destinations:", error);
    }
  };

  const handleItinerary = async (itinerary_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/get-admin-itinerary-data`,
        {
          itinerary_id: itinerary_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      setLoading(false);
      setShowModalData(true);
      setItineraryId(itinerary_id);
      setModalData(response.data.data);
    } catch (error) {
      setLoading(false);
      console.error("Error fetching itinerary data:", error);
      toast.error("Error getting the itinerary, please try again later!!");
    }
  };

  const closeModalData = () => {
    setShowModalData(false);
  };

  const share = (name, destination, mode, startDate, endDate, cost) => {
    navigator.clipboard.writeText(`Hey! Let's go to ${destination}
    Here's the plan
    Name - ${name}
    Destination - ${destination}
    Mode - ${mode}
    From - ${moment(startDate).utc().format("DD-MM-YYYY")},
    To - ${moment(endDate).utc().format("DD-MM-YYYY")},
    Estimated Cost - ${cost},
    Would you like to join?
    `);

    toast.success("Copied to clipboard!");
  };

  const handleSearch = (query) => {
    const filtered = allItineraries.filter((itinerary) => {
      const planName = itinerary.planName
        ? itinerary.planName.toLowerCase()
        : "";
      const destination = itinerary.destination
        ? itinerary.destination.toLowerCase()
        : "";
      const travelMode = itinerary.travelMode
        ? itinerary.travelMode.toLowerCase()
        : "";
      const estimatedCost = itinerary.estimatedCost
        ? itinerary.estimatedCost.toString().toLowerCase()
        : "";
      return (
        planName.includes(query.toLowerCase()) ||
        destination.includes(query.toLowerCase()) ||
        travelMode.includes(query.toLowerCase()) ||
        estimatedCost.includes(query.toLowerCase())
      );
    });
    const savedItinerariesIds = saved.map((itinerary) => itinerary._id);

    const initialHeartStatesFiltered = filtered.map((itinerary) =>
      savedItinerariesIds.includes(itinerary._id)
    );
    setItineraries(filtered);
    setHeartStates(initialHeartStatesFiltered);
  };

  const handleAtlasSearch = async (query) => {
    setSearchQuery(query);
    if (query.trim() !== "") {
      try {
        const response = await axios.post(
          `${BASE_URL}${API_URL}/users/search-destination`,
          { query: query },
          { headers: { Authorization: "Bearer " + token } }
        );
        setFilteredDestinations(response.data.results);
      } catch (error) {
        console.error("Error fetching suggestions:", error);
      }
    } else {
      setFilteredDestinations(allDestinations);
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setSearchCriteria((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSearchDate = async () => {
    try {
      console.log("hello");
      const { startDate, endDate, cost, source, destination } = searchCriteria;
      const filteredItineraries = allItineraries.filter((itinerary) => {
        const travelStartDate = moment(itinerary.travelStartDate)
          .utc()
          .format("YYYY-MM-DD");
        const travelEndDate = moment(itinerary.travelEndDate)
          .utc()
          .format("YYYY-MM-DD");
        const itineraryCost = parseInt(itinerary.estimatedCost);
        console.log(itineraryCost);
        return (
          (moment(travelStartDate).isSameOrAfter(startDate) &&
            moment(travelEndDate).isSameOrBefore(endDate)) ||
          itinerary.planName.toLowerCase() == source.toLowerCase() ||
          itinerary.destination.toLowerCase() == destination.toLowerCase() ||
          itineraryCost === parseInt(cost)
        );
      });

      const savedItinerariesIds = saved.map((destination) => destination._id);
      const initialHeartStatesFiltered = filteredItineraries.map((itinerary) =>
        savedItinerariesIds.includes(itinerary._id)
      );
      setItineraries(filteredItineraries);
      setHeartStates(initialHeartStatesFiltered);
      console.log(filteredItineraries);
    } catch (error) {
      console.error("Error searching itineraries:", error);
    }
  };

  const handleRemoveFilter = () => {
    setSearchCriteria({
      startDate: "",
      endDate: "",
      cost: "",
      source: "",
      destination: "",
    });

    const savedItinerariesIds = saved.map((destination) => destination._id);
    const initialHeartStatesFiltered = allItineraries.map((itinerary) =>
      savedItinerariesIds.includes(itinerary._id)
    );
    setItineraries(allItineraries);
    setHeartStates(initialHeartStatesFiltered);
  };

  return (
    <div>
      <Head>
        <title>Explore Itineraries | Travel Pro</title>
        <meta name="description" content="Travel Pro Itinerary"></meta>
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8"></meta>

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>
        <div>
          <div>
            <Sidebar setLoading={setLoading} />
          </div>
          <div className="destination_search_div">
            <input
              type="text"
              placeholder="Search itineraries..."
              onChange={(e) => handleSearch(e.target.value)}
            />
          </div>

          <div className="destination_search_filter_div">
            <div>
              <input
                type="date"
                name="startDate"
                placeholder="Start Date"
                value={searchCriteria.startDate}
                onChange={handleInputChange}
              />

              <input
                type="date"
                name="endDate"
                placeholder="End Date"
                value={searchCriteria.endDate}
                onChange={handleInputChange}
              />

              <input
                type="number"
                name="cost"
                placeholder="Cost"
                value={searchCriteria.cost}
                onChange={handleInputChange}
              />

              <input
                type="text"
                name="source"
                placeholder="Source"
                value={searchCriteria.source}
                onChange={handleInputChange}
              />

              <input
                type="text"
                name="destination"
                placeholder="Destination"
                value={searchCriteria.destination}
                onChange={handleInputChange}
              />
            </div>
            <div>
              <button onClick={() => handleSearchDate()}>Search</button>
              <button onClick={() => handleRemoveFilter()}>
                Remove Filter
              </button>
            </div>
          </div>

          <div className="itinerary_main_div">
            {itineraries && itineraries.length != 0 ? (
              itineraries.map((ai, index) => {
                return (
                  <div
                    className="itinerary_box_div"
                    key={ai._id}
                    data-bs-target={showModal ? "#exampleModal" : ""}
                  >
                    <div onClick={() => handleItinerary(ai._id)}>
                      <p>{ai.planName}</p>
                      <p>Mode: {ai.travelMode}</p>
                      <p>
                        From:
                        {moment(ai.travelStartDate).utc().format("DD-MM-YYYY")}
                      </p>
                      <p>
                        To:{" "}
                        {moment(ai.travelEndDate).utc().format("DD-MM-YYYY")}
                      </p>
                    </div>

                    <div className="name_travel_favourite_div">
                      <div>
                        <p>Destination: {ai.destination}</p>
                        <p className="avgTravel_div">
                          Cost: ₹{ai.estimatedCost}
                        </p>
                      </div>
                    </div>

                    <div className="share_icon_explore_div">
                      <div
                        onClick={() => handleButtonClick(index, ai._id)}
                        className={`HeartAnimation ${
                          heartStates[index] ? "animate" : ""
                        }`}
                      ></div>
                      <div
                        onClick={() =>
                          share(
                            ai.planName,
                            ai.destination,
                            ai.travelMode,
                            ai.travelStartDate,
                            ai.travelEndDate,
                            ai.estimatedCost
                          )
                        }
                        class="share-icon"
                      ></div>
                    </div>
                  </div>
                );
              })
            ) : (
              <div className="no_itineraries">
                <p>No Itineraries</p>
              </div>
            )}
          </div>

          <Modal
            size="sm"
            show={showModalData}
            onHide={closeModalData}
            aria-labelledby="example-custom-modal-styling-title"
            centered
          >
            <ItineraryDataModal modalData={modalData} />
          </Modal>
        </div>
      </div>
      <Toast />
      {loading && <Loading />}
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const res = await axios.get(
      `${BASE_URL}${API_URL}/users/get-all-admin-itinerary`,
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );

    const resSaved = await axios.post(
      `${BASE_URL}${API_URL}/users/get-saved-itineraries`,
      {
        user_id: context.req.cookies.user_id,
      },
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );
    const resItinerary = res.data.data;
    const resSavedItinerary = resSaved.data.itineraries;
    return {
      props: {
        allItineraries: resItinerary,
        savedItineraries: resSavedItinerary,
      },
    };
  } catch (err) {
    console.log(err.response.status);
    if (err.response.status == 500) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else if (err.response.status == 401 || 403) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else {
      return {
        props: {
          error: [
            {
              error: "retry",
            },
          ],
        },
      };
    }
  }
}
