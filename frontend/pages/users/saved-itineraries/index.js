import Head from "next/head";
import { useRouter } from "next/router";
import axios from "axios";
import Sidebar from "../../../Components/Sidebar";
import { BASE_URL, API_URL } from "../../../constants";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import moment from "moment";
import ItineraryDataModal from "../../../Components/itineraryDataModal";
import Loading from "../../../Components/Loading";
import Modal from "react-bootstrap/Modal";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";

export default function SavedItineraries({ savedItineraires }) {
  const router = useRouter();

  const [token, setToken] = useState("");
  const [id, setId] = useState("");
  const [showModalData, setShowModalData] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalData, setModalData] = useState(null);
  const [itineraryId, setItineraryId] = useState("");
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const tokenFromCookie = Cookies.get("token");
    const idFromCookie = Cookies.get("user_id");
    setToken(tokenFromCookie || "");
    setId(idFromCookie || "");
  }, []);

  const closeModalData = () => {
    setShowModalData(false);
  };

  const handleItinerary = async (itinerary_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/get-admin-itinerary-data`,
        {
          itinerary_id: itinerary_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      setLoading(false);
      setShowModalData(true);
      setItineraryId(itinerary_id);
      setModalData(response.data.data);
    } catch (error) {
      setLoading(false);
      toast.error("Error getting the itinerary, please try again later!!");
      console.error("Error fetching itinerary data:", error);
    }
  };

  return (
    <div>
      <Head>
        <title>Saved Itineraries | Travel Pro</title>
        <meta name="description" content="Travel Pro Saved Itineraries"></meta>
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>
        <div>
          <div>
            <Sidebar setLoading={setLoading} />
          </div>
          <div className="destination_main_div">
            {savedItineraires.map((ai, index) => {
              return (
                <div
                  key={ai._id}
                  className="itinerary_box_div"
                  data-bs-target={showModal ? "#exampleModal" : ""}
                >
                  <div onClick={() => handleItinerary(ai._id)}>
                    <p>{ai.planName}</p>
                    <p>Mode: {ai.travelMode}</p>
                    <p>
                      From:
                      {moment(ai.travelStartDate).utc().format("DD-MM-YYYY")}
                    </p>
                    <p>
                      To: {moment(ai.travelEndDate).utc().format("DD-MM-YYYY")}
                    </p>
                  </div>

                  <div className="name_travel_favourite_div">
                    <div>
                      <p>Destination: {ai.destination}</p>
                      <p className="avgTravel_div">Cost: ₹{ai.estimatedCost}</p>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <Modal
          size="sm"
          show={showModalData}
          onHide={closeModalData}
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <ItineraryDataModal modalData={modalData} />
        </Modal>

        <Toast />

        {loading && <Loading />}
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const resSaved = await axios.post(
      `${BASE_URL}${API_URL}/users/get-saved-itineraries`,
      {
        user_id: context.req.cookies.user_id,
      },
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );
    const resSavedItinerary = resSaved.data.itineraries;
    return {
      props: {
        savedItineraires: resSavedItinerary,
      },
    };
  } catch (err) {
    console.log(err.response.status);
    if (err.response.status == 500) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else if (err.response.status == 401 || 403) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else {
      return {
        props: {
          error: [
            {
              error: "retry",
            },
          ],
        },
      };
    }
  }
}
