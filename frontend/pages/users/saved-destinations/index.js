import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import axios from "axios";
import Sidebar from "../../../Components/Sidebar";
import { BASE_URL, API_URL } from "../../../constants";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import DestinationModal from "../../../Components/destinationModal";
import ReviewModal from "../../../Components/reviewModal";
import Modal from "react-bootstrap/Modal";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";
import Loading from "../../../Components/Loading";

export default function SavedDestinaitons({ savedDestinations }) {
  const router = useRouter();

  const [token, setToken] = useState("");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [modalData, setModalData] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [reviewModal, setReviewModal] = useState(false);
  const [reviews, setReviews] = useState([]);
  const [destinationId, setDestinationId] = useState("");
  const [review, setReview] = useState("");
  const [rating, setRating] = useState(0);

  useEffect(() => {
    const tokenFromCookie = Cookies.get("token");
    const idFromCookie = Cookies.get("user_id");
    const nameFromCookie = Cookies.get("username");
    setToken(tokenFromCookie || "");
    setId(idFromCookie || "");
    setName(nameFromCookie || "");
  }, []);

  const handleRating = (rate) => {
    setRating(rate);
  };

  const handleReview = async () => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/add-review`,
        {
          name: name,
          review: review,
          rating: rating,
          destination_id: destinationId,
          user_id: id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      if (response.status == 200) {
        setLoading(false);
        setReviews([...reviews, response.data.review]);
        setReviewModal(false);
        toast.success("Review Added");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error fetching destination data:", error);
      toast.error("Error adding the review, please try again later!!");
    }
  };

  const handleDestination = async (destination_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/get-destination`,
        {
          destination_id: destination_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      setLoading(false);
      setDestinationId(destination_id);
      setModalData(response.data.data.destination);
      setShowModal(true);
      setReviews(response.data.data.reviews);
    } catch (error) {
      setLoading(false);
      console.error("Error fetching destination data:", error);
    }
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const closeReviewModal = () => {
    setReviewModal(false);
  };

  return (
    <div>
      <Head>
        <title>Saved Destinations | Travel Pro</title>
        <meta name="description" content="Travel Pro Saved Destinations"></meta>
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>
        <div>
          <div>
            <Sidebar setLoading={setLoading} />
          </div>
          <div className="destination_main_div">
            {savedDestinations &&
              savedDestinations?.map((ad, index) => {
                return (
                  <div className="destination_image_div" key={ad._id}>
                    <div>
                      <Image
                        className="destination_image"
                        src={ad.images[0]}
                        width={300}
                        height={230}
                        onClick={() => handleDestination(ad._id)}
                        alt="destination"
                      />
                    </div>

                    <div className="name_travel_favourite_div">
                      <div>
                        <p>{ad.name}</p>
                        <p className="avgTravel_div">
                          ₹{ad.avgTravelExpenses}/night
                        </p>
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
        </div>
        <Modal
          size="xl"
          show={showModal}
          onHide={closeModal}
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <DestinationModal
            modalData={modalData}
            reviews={reviews}
            setReviewModal={setReviewModal}
            name={name}
          />
        </Modal>

        <Modal
          size="sm"
          show={reviewModal}
          onHide={closeReviewModal}
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <ReviewModal
            review={review}
            setReview={setReview}
            handleRating={handleRating}
            handleReview={handleReview}
          />
        </Modal>
        <Toast />

        {loading && <Loading />}
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const resSaved = await axios.post(
      `${BASE_URL}${API_URL}/users/get-saved-destinations`,
      {
        user_id: context.req.cookies.user_id,
      },
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );
    const resSavedDestination = resSaved.data.destinations;
    return {
      props: {
        savedDestinations: resSavedDestination,
      },
    };
  } catch (err) {
    console.log(err.response.status);
    if (err.response.status == 500) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else if (err.response.status == 401 || 403) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else {
      return {
        props: {
          error: [
            {
              error: "retry",
            },
          ],
        },
      };
    }
  }
}
