import { useState, useEffect } from "react";
import Loading from "../../../Components/Loading";
import Sidebar from "../../../Components/Sidebar";
import DestinationModal from "../../../Components/destinationModal";
import ReviewModal from "../../../Components/reviewModal";
import Image from "next/image";
import Modal from "react-bootstrap/Modal";
import Cookies from "js-cookie";
import axios, { all } from "axios";
import { BASE_URL, API_URL } from "../../../constants";
import Head from "next/head";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";
import { Configuration, OpenAIApi } from "openai";
import ChatBot from "../../../Components/ChatBot";
// import socketIOClient from "socket.io-client";

const configuration = new Configuration({
  apiKey: process.env.NEXT_PUBLIC_OPENAI_API_URL,
});
const openai = new OpenAIApi(configuration);

export default function Dashboard({
  destinations,
  navbar,
  allDestinations,
  savedDestinations,
}) {
  const [token, setToken] = useState("");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [heartStates, setHeartStates] = useState([]);
  const [modalData, setModalData] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [reviewModal, setReviewModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");
  const [destinationId, setDestinationId] = useState("");
  const [reviews, setReviews] = useState([]);
  const [filteredDestinations, setFilteredDestinations] =
    useState(allDestinations);
  const [saved, setSaved] = useState(savedDestinations);
  const [queryValue, setQueryValue] = useState("");

  useEffect(() => {
    const tokenFromCookie = Cookies.get("token");
    const idFromCookie = Cookies.get("user_id");
    const nameFromCookie = Cookies.get("username");
    setToken(tokenFromCookie || "");
    setId(idFromCookie || "");
    setName(nameFromCookie || "");
    fetchSavedDestinations();
  }, []);

  useEffect(() => {
    const savedDestinationIds = saved.map((destination) => destination._id);
    const initialHeartStatesFiltered = filteredDestinations.map((destination) =>
      savedDestinationIds.includes(destination._id)
    );
    setHeartStates(initialHeartStatesFiltered);
  }, [saved, filteredDestinations]); // Add saved and filteredDestinations as dependencies

  const fetchSavedDestinations = async () => {
    try {
      setLoading(true);
      const savedDestinationIds = savedDestinations?.map(
        (destination) => destination._id
      );
      const initialHeartStates = allDestinations?.map((destination) =>
        savedDestinationIds.includes(destination._id)
      );
      setLoading(false);
      setHeartStates(initialHeartStates);
    } catch (error) {
      setLoading(false);
      console.error("Error fetching saved destinations:", error);
    }
  };

  const handleButtonClick = async (index, destination_id) => {
    try {
      setLoading(true);
      setHeartStates((prevHeartStates) => {
        const newHeartStates = [...prevHeartStates];
        newHeartStates[index] = !newHeartStates[index];
        return newHeartStates;
      });
      const data = await axios.post(
        `${BASE_URL}${API_URL}/users/save-destination`,
        {
          destination_id: destination_id,
          user_id: id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      if (data.status == 200) {
        setLoading(false);
        if (data.data.message == "Added to your saved destination list") {
          setSaved((prevSaved) => [...prevSaved, { _id: destination_id }]);
          toast.success("Added to your saved destination list");
        } else {
          setSaved((prevSaved) =>
            prevSaved.filter(
              (destination) => destination._id !== destination_id
            )
          );
          toast.success("Removed from your saved destination list");
        }
      }
    } catch (error) {
      setLoading(false);
      toast.error("Error in saving the destination list");
      console.error("Error fetching destination data:", error);
    }
  };

  const handleAtlasSearch = async (query) => {
    if (query.trim !== "" && query.trim().length > 2) {
      setLoading(true);
      try {
        const response = await axios.post(
          `${BASE_URL}${API_URL}/users/search-destination`,
          { query: query },
          { headers: { Authorization: "Bearer " + token } }
        );
        if (response.status == 200) {
          setLoading(false);
          setFilteredDestinations(response.data.results);
        }
      } catch (error) {
        setFilteredDestinations(allDestinations);
        console.error("Error fetching suggestions:", error);
      }
    } else {
      setFilteredDestinations(allDestinations);
    }
  };

  const handleDestination = async (destination_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/get-destination`,
        {
          destination_id: destination_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      setLoading(false);
      setDestinationId(destination_id);
      setModalData(response.data.data.destination);
      setShowModal(true);
      setReviews(response.data.data.reviews);
    } catch (error) {
      setLoading(false);
      console.error("Error fetching destination data:", error);
      toast.error("Error getting the destination, please try again later!!");
    }
  };

  const handleRating = (rate) => {
    setRating(rate);
  };

  const handleReview = async () => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/users/add-review`,
        {
          name: name,
          review: review,
          rating: rating,
          destination_id: destinationId,
          user_id: id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      if (response.status == 200) {
        setLoading(false);
        setReviews([...reviews, response.data.review]);
        setReviewModal(false);
        toast.success("Review Added");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error fetching destination data:", error);
      toast.error("Error adding the review, please try later");
    }
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const closeReviewModal = () => {
    setReviewModal(false);
  };

  const handleSearch = async (query) => {
    try {
      setLoading(true);
      const filtered = allDestinations.filter((destination) => {
        const name = destination.name ? destination.name.toLowerCase() : "";
        const city = destination.city ? destination.city.toLowerCase() : "";
        const state = destination.state ? destination.state.toLowerCase() : "";
        const landmarks = destination.landmarks
          ? destination.landmarks.join(", ").toLowerCase()
          : "";
        const attractions = destination.attractions
          ? destination.attractions.join(", ").toLowerCase()
          : "";
        const category = destination.category
          ? destination.category.toLowerCase()
          : "";
        return (
          name.includes(query.toLowerCase()) ||
          city.includes(query.toLowerCase()) ||
          state.includes(query.toLowerCase()) ||
          landmarks.includes(query.toLowerCase()) ||
          attractions.includes(query.toLowerCase()) ||
          category.includes(query.toLowerCase())
        );
      });

      const savedItinerariesIds = saved.map((destination) => destination._id);
      const initialHeartStatesFiltered = filteredItineraries.map((itinerary) =>
        savedItinerariesIds.includes(itinerary._id)
      );
      setItineraries(filteredItineraries);
      setHeartStates(initialHeartStatesFiltered);
      console.log(filteredItineraries);

      setFilteredDestinations(filtered);
      setLoading(false);
    } catch (error) {
      console.error("Error during search:", error);
      setLoading(false);
    }
  };

  return (
    <div>
      <navbar />
      <Head>
        <title>Dashboard | Travel Pro</title>
        <meta name="description" content="Travel Pro Dashboard"></meta>
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8"></meta>

        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        <Sidebar setLoading={setLoading} />
        <div className="destination_search_div">
          <input
            type="text"
            placeholder="Type first word of the place..."
            onChange={(e) => handleAtlasSearch(e.target.value)}
          />
        </div>
        <div className="destination_main_div">
          {filteredDestinations && filteredDestinations.length != 0 ? (
            filteredDestinations.map((ad, index) => (
              <div className="destination_image_div" key={ad._id}>
                <div>
                  <Image
                    className="destination_image"
                    src={ad.images[0]}
                    width={300}
                    height={230}
                    onClick={() => handleDestination(ad._id)}
                    alt="destination"
                  />
                </div>
                <div className="name_travel_favourite_div">
                  <div>
                    <p>{ad.name}</p>
                    <p className="avgTravel_div">
                      ₹{ad.avgTravelExpenses}/night
                    </p>
                  </div>
                  <div
                    onClick={() => handleButtonClick(index, ad._id)}
                    className={`HeartAnimation ${
                      heartStates ? (heartStates[index] ? "animate" : "") : ""
                    }`}
                  ></div>
                </div>
              </div>
            ))
          ) : (
            <div className="no_itineraries">
              <p>No Destinations</p>
            </div>
          )}
        </div>
      </div>

      <Modal
        size="xl"
        show={showModal}
        onHide={closeModal}
        aria-labelledby="example-custom-modal-styling-title"
        centered
      >
        <DestinationModal
          modalData={modalData}
          reviews={reviews}
          setReviewModal={setReviewModal}
          name={name}
        />
      </Modal>

      <Modal
        size="sm"
        show={reviewModal}
        onHide={closeReviewModal}
        aria-labelledby="example-custom-modal-styling-title"
        centered
      >
        <ReviewModal
          review={review}
          setReview={setReview}
          handleRating={handleRating}
          handleReview={handleReview}
        />
      </Modal>
      <Toast />

      <ChatBot />

      {loading && <Loading />}
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const res = await axios.get(
      `${BASE_URL}${API_URL}/users/get-all-destinations`,
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );

    const resSaved = await axios.post(
      `${BASE_URL}${API_URL}/users/get-saved-destinations`,
      {
        user_id: context.req.cookies.user_id,
      },
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );
    const resDestination = res.data.data;
    const resSavedDestination = resSaved.data.destinations;
    return {
      props: {
        allDestinations: resDestination,
        savedDestinations: resSavedDestination,
      },
    };
  } catch (err) {
    console.log(err.response.status);
    if (err.response.status == 500) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else if (err.response.status == 401 || 403) {
      return {
        redirect: {
          destination: "/users/login",
        },
      };
    } else {
      return {
        props: {
          error: [
            {
              error: "retry",
            },
          ],
        },
      };
    }
  }
}
