import '../styles/globals.css'
import '../styles/home.css' 
import '../styles/auth.css'
import '../styles/dashboard.css'
import '../styles/itinerary.css'
import '../styles/loading.css'
import '../styles/maps.css'
import '../styles/adminDashboard.css'
import '../styles/chatBot.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
