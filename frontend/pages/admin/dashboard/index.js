import { useState, useEffect } from "react";
import Loading from "../../../Components/Loading";
import SidebarAdmin from "../../../Components/SidebarAdmin";
import AdminDestinationModal from "../../../Components/AdminDestinationModal";
import DestinationUpdateModal from "../../../Components/DestinationUpdateModal";
import Image from "next/image";
import Modal from "react-bootstrap/Modal";
import Cookies from "js-cookie";
import axios from "axios";
import { BASE_URL, API_URL } from "../../../constants";
import Head from "next/head";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";
import AddDestinationModal from "../../../Components/AddDestinationModal";
import { MdDelete } from "react-icons/md";
import { FiEdit } from "react-icons/fi";

export default function AdminDashboard({ allDestinations }) {
  const [token, setToken] = useState("");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [modalData, setModalData] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [destinationId, setDestinationId] = useState("");
  const [reviews, setReviews] = useState([]);
  const [images, setImages] = useState([]);
  const [filteredDestinations, setFilteredDestinations] =
    useState(allDestinations);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [formData, setFormData] = useState({
    name: "",
    city: "",
    landmarks: [],
    state: "",
    description: "",
    avgTravelExpenses: "",
    attractions: [],
    category: "",
  });

  const [updateFormData, setUpdateFormData] = useState({
    name: "",
    city: "",
    landmarks: [],
    state: "",
    description: "",
    avgTravelExpenses: "",
    attractions: [],
    category: "",
    images: [],
  });

  useEffect(() => {
    const tokenFromCookie = Cookies.get("token");
    const idFromCookie = Cookies.get("user_id");
    const nameFromCookie = Cookies.get("username");
    setToken(tokenFromCookie || "");
    setId(idFromCookie || "");
    setName(nameFromCookie || "");
  }, []);

  const handleDestination = async (destination_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/admin/get-destination`,
        {
          destination_id: destination_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      setLoading(false);
      setDestinationId(destination_id);
      setModalData(response.data.data.destination);
      setShowModal(true);
      setReviews(response.data.data.reviews);
    } catch (error) {
      setLoading(false);
      toast.error("Error getting the destination please try again later!!");
      console.error("Error fetching destination data:", error);
    }
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const closeAddModal = () => {
    setShowAddModal(false);
  };

  const closeUpdateModalData = () => {
    setShowUpdateModal(false);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "attractions" || name === "landmarks") {
      // Split the comma-separated string into an array
      const arrValue = value.split(",").map((item) => item.trim());
      setFormData((prevFormData) => ({
        ...prevFormData,
        [name]: arrValue,
      }));
    } else {
      setFormData((prevFormData) => ({
        ...prevFormData,
        [name]: value,
      }));
    }
  };

  const updateHandleChange = (e) => {
    const { name, value } = e.target;
    if (name === "attractions" || name === "landmarks") {
      // Split the comma-separated string into an array
      const arrValue = value.split(",").map((item) => item.trim());
      setUpdateFormData((prevFormData) => ({
        ...prevFormData,
        [name]: arrValue,
      }));
    } else {
      setUpdateFormData((prevFormData) => ({
        ...prevFormData,
        [name]: value,
      }));
    }
  };

  const handleSubmit = async () => {
    try {
      setLoading(true);
      if (
        !formData.name ||
        !formData.city ||
        !formData.state ||
        !formData.attractions ||
        !formData.avgTravelExpenses ||
        !formData.category ||
        !formData.description ||
        !formData.landmarks
      ) {
        setLoading(false);
        toast.error("Details required");
      } else {
        const response = await axios.post(
          `${BASE_URL}${API_URL}/admin/add-destination`,
          {
            ...formData,
            images: images,
          },
          { headers: { Authorization: "Bearer " + token } }
        );

        if (response.status == 200) {
          setLoading(false);
          console.log(response.data.savedDestination);
          setFilteredDestinations([
            ...filteredDestinations,
            response.data.savedDestination,
          ]);
          toast.success("Destination created successfully");
          setShowAddModal(false);
        }
      }
    } catch (err) {
      setLoading(false);
      toast.error(
        "Error adding the destination please fill all the details properly!!"
      );
    }
  };

  // Function to handle image change
  const handleImageChange = (e) => {
    const files = e.target.files;
    // Convert FileList to an array
    const selectedImages = Array.from(files);
    setImages((prevImages) => [...prevImages, ...selectedImages]);
    console.log(images);
  };

  const uploadImages = async () => {
    try {
      setLoading(true);

      const formData = new FormData(); // Create a FormData object

      // Append each image to the FormData object
      images.forEach((image, index) => {
        formData.append(`files`, image);
      });
      // Make a POST request to upload the images
      const response = await axios.post(
        `${BASE_URL}${API_URL}/admin/upload-files`,
        formData,
        {
          headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "multipart/form-data",
          },
        }
      );

      // Handle the response
      if (response.status === 200) {
        setLoading(false);
        console.log(response.data.message);
        setImages(response.data.filesUrls);
        toast.success("Images uploaded successfully");
      } else {
        setLoading(false);
        toast.error("Please select images");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error uploading images:", error);
      toast.error("Failed to upload images");
    }
  };

  function extractFilenamesFromUrls(images) {
    return images.map(url => {
      // Split the URL by "/" and get the last part
      const parts = url.split("/");
      const filenameWithExtension = parts[parts.length - 1];
      
      // If filename has a query string (like "file.jpeg?12345"), remove it
      const filenameWithoutQueryString = filenameWithExtension.split("?")[0];
  
      return filenameWithoutQueryString;
    });
  }

  const deleteObjects = async (images) => {
    try {
      setLoading(true);
      const filename = extractFilenamesFromUrls(images);
      console.log(token);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/admin/delete-files`,
        {
          keys: filename,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`, // Add the token to the Authorization header
          },
        }
      );
      if (response.status == 200) {
        setLoading(false);
        console.log("deleted successfully");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error deleting destination:", error);
      toast.error("Error deleting the images from s3, please try again later!!");
    }
  };

  const deleteDestination = async (destination_id) => {
    try {
      setLoading(true);
      const response = await axios.delete(
        `${BASE_URL}${API_URL}/admin/delete-destination`,
        {
          headers: {
            Authorization: "Bearer " + token,
          },
          data: {
            destination_id: destination_id,
          },
        }
      );

      if (response.status === 200) {
        setLoading(false);
        await deleteObjects(response.data.data.images);
        // Assuming response.data.data contains the deleted itinerary
        // Filter out the deleted itinerary from the itineraries array
        const updatedDestinations = filteredDestinations.filter(
          (destination) => destination._id !== destination_id
        );
        setFilteredDestinations(updatedDestinations);
        toast.success("Destination deleted successfully");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error deleting destination:", error);
      toast.error("Error deleting the destination, please try again later!!");
    }
  };

  const updateDestination = async () => {
    try {
      setLoading(true);

      const response = await axios.put(
        `${BASE_URL}${API_URL}/admin/update-destination`,
        {
          destination_id: destinationId,
          updatedData: { ...updateFormData },
        },
        { headers: { Authorization: "Bearer " + token } }
      );

      if (response.status == 200) {
        const updatedDestination = response.data.data;
        console.log(updatedDestination);
        const updatedDestinations = filteredDestinations.map((destination) =>
          destination._id === updatedDestination._id
            ? updatedDestination
            : destination
        );

        setFilteredDestinations(updatedDestinations);
        setLoading(false);
        // setItineraries([...itineraries, response.data.data]);
        toast.success("Destination updated successfully");
        setShowUpdateModal(false);
      }
    } catch (err) {
      setLoading(false);
      toast.error("Error updating the destination, please try again later!!");
    }
  };

  const update = async (destination_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/admin/get-destination`,
        {
          destination_id: destination_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      if (response.status == 200) {
        setDestinationId(destination_id);
        const destination = response.data.data.destination;
        console.log(destination);
        setLoading(false);
        setUpdateFormData({
          name: destination.name,
          city: destination.city,
          state: destination.state,
          landmarks: destination.landmarks,
          description: destination.description,
          avgTravelExpenses: destination.avgTravelExpenses,
          category: destination.category,
          attractions: destination.attractions,
          images: destination.images,
        });
        // console.log(updateFormData);
        setShowUpdateModal(true);
      } else {
        setLoading(false);
        toast.error("Error getting the data, please try again later!!");
      }
    } catch (error) {
      setLoading(false);
      toast.error("Error getting the data, please try again later!!");
    }
  };

  return (
    <div>
      <Head>
        <title>Admin Dashboard | Travel Pro</title>
        <meta name="description" content="Travel Pro Admin Dashboard"></meta>
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8"></meta>

        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        <SidebarAdmin setLoading={setLoading} />

        <div className="destination_main_div">
          {filteredDestinations && filteredDestinations.length != 0 ? (
            filteredDestinations.map((ad, index) => (
              <div className="destination_image_div" key={ad._id}>
                <div>
                  <Image
                    className="destination_image"
                    src={ad.images[0]}
                    width={300}
                    height={230}
                    onClick={() => handleDestination(ad._id)}
                    alt="destination"
                  />
                </div>
                <div className="name_travel_favourite_div">
                  <div>
                    <p>{ad.name}</p>
                    <p className="avgTravel_div">
                      ₹{ad.avgTravelExpenses}/night
                    </p>
                  </div>
                </div>
                <div className="update_delete_functionality_div">
                  <div>
                    <MdDelete
                      onClick={() => deleteDestination(ad._id)}
                      size={30}
                    />
                  </div>

                  <div>
                    <FiEdit onClick={() => update(ad._id)} size={30} />
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="no_itineraries">
              <p>No Destinations</p>
            </div>
          )}
        </div>
      </div>

      <Modal
        size="xl"
        show={showModal}
        onHide={closeModal}
        aria-labelledby="example-custom-modal-styling-title"
        centered
      >
        <AdminDestinationModal modalData={modalData} name={name} />
      </Modal>

      <Modal
        size="xl"
        show={showAddModal}
        onHide={closeAddModal}
        aria-labelledby="example-custom-modal-styling-title"
        centered
      >
        <AddDestinationModal
          formData={formData}
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          handleImageChange={handleImageChange}
          uploadImages={uploadImages}
          images={images}
        />
      </Modal>

      <Modal
        size="xl"
        show={showUpdateModal}
        onHide={closeUpdateModalData}
        aria-labelledby="example-custom-modal-styling-title"
        centered
      >
        <DestinationUpdateModal
          updateFormData={updateFormData}
          updateHandleChange={updateHandleChange}
          updateDestination={updateDestination}
        />
      </Modal>

      <Toast />

      <div
        onClick={() => {
          setShowAddModal(true);
        }}
        data-bs-target={showAddModal ? "#exampleModal" : ""}
        class="plus-icon"
      >
        +
      </div>

      {loading && <Loading />}
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const res = await axios.get(
      `${BASE_URL}${API_URL}/admin/get-all-destination`,
      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );

    const resDestination = res.data.data;
    return {
      props: {
        allDestinations: resDestination,
      },
    };
  } catch (err) {
    console.log(err.response.status);
    if (err.response.status == 500) {
      return {
        redirect: {
          destination: "/admin/login",
        },
      };
    } else if (err.response.status == 401 || 403) {
      return {
        redirect: {
          destination: "/admin/login",
        },
      };
    } else {
      return {
        props: {
          error: [
            {
              error: "retry",
            },
          ],
        },
      };
    }
  }
}
