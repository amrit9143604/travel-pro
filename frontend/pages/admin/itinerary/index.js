import Head from "next/head";
import { useRouter } from "next/router";
import axios from "axios";
import SidebarAdmin from "../../../Components/SidebarAdmin";
import { BASE_URL, API_URL } from "../../../constants";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import Modal from "react-bootstrap/Modal";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Toast from "../../../Components/Toast";
import moment from "moment";
import Loading from "../../../Components/Loading";
import ItineraryModal from "../../../Components/itineraryModal";
import ItineraryDataModal from "../../../Components/itineraryDataModal";
import ItineraryUpdateModal from "../../../Components/ItineraryUpdateModal";
import { MdDelete } from "react-icons/md";
import { FiEdit } from "react-icons/fi";

export default function Itinerary({ allItineraries }) {
  const router = useRouter();

  const [token, setToken] = useState("");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [itineraryId, setItineraryId] = useState("");
  const [rows, setRows] = useState([{ id: 1, location: "", date: "" }]);
  const [displayedValues, setDisplayedValues] = useState([]);
  const [updatedDisplayedValues, setUpdatedDisplayedValues] = useState([]);
  const [modalData, setModalData] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showModalData, setShowModalData] = useState(false);
  const [loading, setLoading] = useState(false);
  const [detailsArray, setDetailsArray] = useState([]);
  const [updatedDetailsArray, setUpdatedDetailsArray] = useState([]);
  const [itineraries, setItineraries] = useState(allItineraries);
  const [formData, setFormData] = useState({
    planName: "",
    travelMode: "",
    travelStartDate: "",
    destination: "",
    estimatedCost: "",
    travelEndDate: "",
  });

  const [updateFormData, setUpdateFormData] = useState({
    planName: "",
    travelMode: "",
    travelStartDate: "",
    destination: "",
    estimatedCost: "",
    travelEndDate: "",
  });

  useEffect(() => {
    const tokenFromCookie = Cookies.get("token");
    const idFromCookie = Cookies.get("user_id");
    const nameFromCookie = Cookies.get("username");
    setToken(tokenFromCookie || "");
    setId(idFromCookie || "");
    setName(nameFromCookie || "");
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const updateHandleChange = (e) => {
    const { name, value } = e.target;
    setUpdateFormData({
      ...updateFormData,
      [name]: value,
    });
  };

  const addRow = () => {
    const newRow = {
      id: rows.length + 1,
      location: "",
      date: "",
    };
    setRows([...rows, newRow]);
  };

  const removeRow = (id) => {
    const updatedRows = rows.filter((row) => row.id !== id);
    setRows(updatedRows);
  };

  const displayValues = (id) => {
    const displayedValue = rows.find((row) => row.id === id);
    if (displayedValue) {
      setDisplayedValues([
        ...displayedValues,
        `${moment(displayedValue.date).format(
          "DD-MM-YYYY"
        )} ------------------------------------------------ ${
          displayedValue.location
        }`,
      ]);
      setDetailsArray((prevValues) => [
        ...prevValues,
        { date: displayedValue.date, dayDetails: displayedValue.location },
      ]);
      removeRow(id);
    }
  };

  const updateDisplayValues = (id) => {
    const displayedValue = rows.find((row) => row.id === id);
    if (displayedValue) {
      setUpdatedDisplayedValues([
        ...updatedDisplayedValues,
        `${moment(displayedValue.date).format(
          "DD-MM-YYYY"
        )}  ------------------------------------------------ ${
          displayedValue.location
        }`,
      ]);
      setUpdatedDetailsArray((prevValues) => [
        ...prevValues,
        { date: displayedValue.date, dayDetails: displayedValue.location },
      ]);
      console.log(updatedDisplayedValues);
      removeRow(id);
    }
  };

  const removeDisplayedRow = (index) => {
    const updatedValues = [...displayedValues];
    updatedValues.splice(index, 1);
    setDisplayedValues(updatedValues);
    setUpdatedDetailsArray(updatedValues);
  };

  const handleItinerary = async (itinerary_id) => {
    try {
      setLoading(true);
      const response = await axios.post(
        `${BASE_URL}${API_URL}/admin/get-itinerary`,
        {
          itinerary_id: itinerary_id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
      setLoading(false);
      setShowModalData(true);
      setItineraryId(itinerary_id);
      setModalData(response.data.data);
    } catch (error) {
      setLoading(false);
      toast.error("Error getting the destination, please try again later!!");
      console.error("Error fetching itinerary data:", error);
    }
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const closeModalData = () => {
    setShowModalData(false);
  };

  const closeUpdateModalData = () => {
    setShowUpdateModal(false);
  };

  const handleSubmit = async () => {
    try{
      setLoading(true);
      if (
        !formData.planName ||
        !formData.travelMode ||
        !formData.travelStartDate ||
        !formData.travelEndDate ||
        !formData.destination ||
        !formData.estimatedCost
      ) {
        setLoading(false);
        toast.error("Details required");
      } else {
        const response = await axios.post(
          `${BASE_URL}${API_URL}/admin/add-itinerary`,
          {
            ...formData,
            details: detailsArray,
            createdBy: id,
          },
          { headers: { Authorization: "Bearer " + token } }
        );
  
        if (response.status == 200) {
          setLoading(false);
          console.log(response.data);
          setItineraries([...itineraries, response.data.savedItineary]);
          toast.success("Itinerary created successfully");
          setShowModal(false);
        } else  {
          setLoading(false);
          toast.error("Error creating the itinerary, please check the travel dates once!!");
        }
      }
    }
    catch(error){
      setLoading(false);
      toast.error("Error creating the itinerary, please check the travel dates once!!");
    }
   
  };

  const share = (destination, mode, startDate, endDate, cost) => {
    navigator.clipboard.writeText(`Hey! Let's go to ${destination}
    Here's the plan
    Destination - ${destination}
    Mode - ${mode}
    From - ${moment(startDate).utc().format("DD-MM-YYYY")},
    To - ${moment(endDate).utc().format("DD-MM-YYYY")},
    Estimated Cost - ${cost},
    Would you like to join?
    `);

    toast.success("Copied to clipboard!");
  };

  const deleteItinerary = async (itinerary_id) => {
    try {
      setLoading(true);
      const response = await axios.delete(
        `${BASE_URL}${API_URL}/admin/delete-itinerary`,
        {
          headers: {
            Authorization: "Bearer " + token,
          },
          data: {
            itinerary_id: itinerary_id,
          },
        }
      );

      if (response.status === 200) {
        setLoading(false);
        // Assuming response.data.data contains the deleted itinerary
        // Filter out the deleted itinerary from the itineraries array
        const updatedItineraries = itineraries.filter(
          (itinerary) => itinerary._id !== itinerary_id
        );
        setItineraries(updatedItineraries);
        toast.success("Itinerary deleted successfully");
      } else {
        setLoading(false);
        toast.error("Error deleting the itinerary");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error deleting itinerary:", error);
      toast.error("Error deleting the itinerary");
    }
  };

  const updateItinerary = async () => {
    try{
      setLoading(true);
  
      const response = await axios.put(
        `${BASE_URL}${API_URL}/admin/update-itinerary`,
        {
          itinerary_id: itineraryId,
          updatedData: { ...updateFormData, details: updatedDetailsArray },
          createdBy: id,
        },
        { headers: { Authorization: "Bearer " + token } }
      );
  
      if (response.status == 200) {
        const updatedItinerary = response.data.data;
        const updatedItineraries = itineraries.map((itinerary) =>
          itinerary._id === updatedItinerary._id ? updatedItinerary : itinerary
        );
  
        setItineraries(updatedItineraries);
        setLoading(false);
        // setItineraries([...itineraries, response.data.data]);
        toast.success("Itinerary updated successfully");
        setShowUpdateModal(false);
      } else {
        setLoading(false);
        toast.error("Error in updating itinerary, please try again later!!");
      }

    }catch(error){
      setLoading(false);
      toast.error("Error in updating itinerary, please try again later!!");
    }
  };

  const update = async (itinerary_id) => {
    setDisplayedValues([]);
    setUpdatedDisplayedValues([]);
    setDetailsArray([]);
    setLoading(true);
    const response = await axios.post(
      `${BASE_URL}${API_URL}/admin/get-itinerary`,
      {
        itinerary_id: itinerary_id,
      },
      { headers: { Authorization: "Bearer " + token } }
    );
    if (response.status == 200) {
      setItineraryId(itinerary_id);
      const itinerary = response.data.data;
      setLoading(false);
      setUpdatedDetailsArray(itinerary.details);
      setUpdateFormData({
        planName: itinerary.planName,
        destination: itinerary.destination,
        travelStartDate: moment(itinerary.travelStartDate)
          .utc()
          .format("YYYY-MM-DD"),
        travelEndDate: moment(itinerary.travelEndDate)
          .utc()
          .format("YYYY-MM-DD"),
        travelMode: itinerary.travelMode,
        estimatedCost: itinerary.estimatedCost,
        createdBy: itinerary.createdBy,
      });
      setDisplayedValues(itinerary.details);
      setShowUpdateModal(true);
    }
  };

  return (
    <div>
      <Head>
        <title>Admin Itinerary | Travel Pro</title>
        <meta name="description" content="Travel Pro Itinerary"></meta>
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>
        <div>
          <div>
            <SidebarAdmin setLoading={setLoading} />
          </div>
          <div className="itinerary_main_div">
            {itineraries &&
              itineraries?.map((ai, index) => {
                return (
                  <div
                    className="itinerary_box_div"
                    key={ai._id}
                    data-bs-target={showModal ? "#exampleModal" : ""}
                  >
                    <div onClick={() => handleItinerary(ai._id)}>
                      <p>{ai.planName}</p>
                      <p>Mode: {ai.travelMode}</p>
                      <p>
                        From:{" "}
                        {moment(ai.travelStartDate).utc().format("DD-MM-YYYY")}
                      </p>
                      <p>
                        To:{" "}
                        {moment(ai.travelEndDate).utc().format("DD-MM-YYYY")}
                      </p>
                    </div>

                    <div className="name_travel_favourite_div">
                      <div>
                        <p>Destination: {ai.destination}</p>
                        <p className="avgTravel_div">
                          Cost: ₹{ai.estimatedCost}
                        </p>
                      </div>
                    </div>

                    <div className="share_icon_div">
                      <div
                        onClick={() =>
                          share(
                            ai.destination,
                            ai.travelMode,
                            ai.travelStartDate,
                            ai.travelEndDate,
                            ai.estimatedCost
                          )
                        }
                        class="share-icon"
                      ></div>
                      <div>
                        <MdDelete
                          onClick={() => deleteItinerary(ai._id)}
                          size={30}
                        />
                      </div>

                      <div>
                        <FiEdit onClick={() => update(ai._id)} size={30} />
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
          <Modal
            size="lg"
            show={showModal}
            onHide={closeModal}
            aria-labelledby="example-custom-modal-styling-title"
            centered
          >
            <ItineraryModal
              formData={formData}
              handleChange={handleChange}
              addRow={addRow}
              rows={rows}
              setRows={setRows}
              removeRow={removeRow}
              displayValues={displayValues}
              displayedValues={displayedValues}
              handleSubmit={handleSubmit}
            />
          </Modal>

          <Modal
            size="sm"
            show={showModalData}
            onHide={closeModalData}
            aria-labelledby="example-custom-modal-styling-title"
            centered
          >
            <ItineraryDataModal modalData={modalData} />
          </Modal>

          <Modal
            size="lg"
            show={showUpdateModal}
            onHide={closeUpdateModalData}
            aria-labelledby="example-custom-modal-styling-title"
            centered
          >
            <ItineraryUpdateModal
              updateFormData={updateFormData}
              updateHandleChange={updateHandleChange}
              addRow={addRow}
              rows={rows}
              setRows={setRows}
              removeRow={removeRow}
              updateDisplayValues={updateDisplayValues}
              updatedDisplayedValues={updatedDisplayedValues}
              displayedValues={displayedValues}
              updateItinerary={updateItinerary}
              removeDisplayedRow={removeDisplayedRow}
            />
          </Modal>
        </div>

        <div
          onClick={() => {
            setShowModal(true), setDisplayedValues([]);
          }}
          data-bs-target={showModal ? "#exampleModal" : ""}
          class="plus-icon"
        >
          +
        </div>
      </div>
      <Toast />
      {loading && <Loading />}
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const res = await axios.get(
      `${BASE_URL}${API_URL}/admin/get-all-itinerary`,

      { headers: { Authorization: "Bearer " + context.req.cookies.token } }
    );

    const resItinerary = res.data.data;

    return {
      props: {
        allItineraries: resItinerary,
      },
    };
  } catch (err) {
    console.log(err.response.status);
    if (err.response.status == 500) {
      return {
        redirect: {
          destination: "/admin/login",
        },
      };
    } else if (err.response.status == 401 || 403) {
      return {
        redirect: {
          destination: "/admin/login",
        },
      };
    } else {
      return {
        props: {
          error: [
            {
              error: "retry",
            },
          ],
        },
      };
    }
  }
}
