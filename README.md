# Travel Pro

Explore the world through your travel buddy which will give you close insights about your travel, destination and expenses. *Pack your bags!*

Frontend - https://master.d2zopih13m6rb5.amplifyapp.com (AWS AMPLIFY)\
Backend - https://travel-pro-backend-krwt.onrender.com (RENDER)

## Project Setup

- Detailed documentation: [`documentations`](documentation)

- Sample Credentials\
 (For both admin and user, Login in admin and user through different browsers to check the functionality)\
  `email - amrit@gmail.com`\
  `password - 123456`

### Next js setup

```bash
cd frontend
npm install
npm run dev
```

### Environment Variables

```bash
NEXT_PUBLIC_OPENAI_API_URL = YOUR OPENAI_API_URL
INTEGER_VARIABLE = YOUR_MAX_TOKENS_FROM_API
```

### Node js setup

```bash
cd backend
npm install
npm run dev
```

### Environment Variables

```bash
PORT = YOUR PORT
MONGO_DB_URL = YOUR MONGODB URL
SECRET = SECRET KEY FOR JWT AUTHENTICATION
WEATHER_API_KEY = YOUR_RAPID_API_KEY
WEATHER_API_HOST = YOUR_RAPID_API_HOST
ACCESS_KEY_ID = YOUR_AWS_S3_ACCESS_KEY_ID
SECRET_ACCESS_KEY = YOUR_AWS_S3_SECRET_ACCESS_KEY
REGION = YOUR_AWS_S3_REGION
BUCKET_NAME = YOUR_AWS_S3_BUCKET_NAME
```
