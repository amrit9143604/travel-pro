# Database Schema

## Admin Details

|   \_id   |    name    |   email   |  password  |
| :------: | :--------: | :-------: | :--------: |
| ObjectId |   string   |  string   |   string   |

## Users Details

|   \_id   |    username    |   email   |  password  |
| :------: | :--------: | :-------: | :--------: |
| ObjectId |   string   |  string   |   string   |

## Destination Details

|   \_id   |   name   |   city   |  landmarks  |  latitude  |  longitude  |  state  |  description  |  images  |  avgTravelExpenses  |  attractions  |  category  |  currentWeather  |  weatherIcon  |  temperature  |  reviews  |
| :------: | :------: | :------: | :---------: | :--------: | :---------: | :-----: | :------------: | :------: | :-----------------: | :-----------: | :--------: | :--------------: | :-----------: | :-----------: | :-------: |
| ObjectId |  string  |  string  |   array     |   string   |    string   |  string |     string     |  array   |       string        |     array     |   string   |      string      |    string     |    number     |   array   |

## Itinerary Details

|   \_id   |  planName  |  destination  |  travelStartDate  |  travelEndDate  |  travelMode  |  details  |  createdBy  |  estimatedCost  |
| :------: | :--------: | :-----------: | :---------------: | :-------------: | :----------: | :-------: | :---------: | :-------------: |
| ObjectId |   string   |    string     |       date        |      date       |    string    |  array    |   ObjectId`user`  |     number      |

## Admin Itinerary Details

|   \_id   |  planName  |  destination  |  travelStartDate  |  travelEndDate  |  travelMode  |  details  |  estimatedCost  |
| :------: | :--------: | :-----------: | :---------------: | :-------------: | :----------: | :-------: | :-------------: |
| ObjectId |   string   |    string     |       date        |      date       |    string    |   array   |     number      |


## Saved Destinations Details

|   \_id   |  savedDestinationsList  |  savedBy  |
| :------: | :---------------------: | :-------: |
| ObjectId |         array           | ObjectId`user`  |


## Saved Itineraries Details

|   \_id   |  savedItinerariesList  |  savedBy  |
| :------: | :--------------------: | :-------: |
| ObjectId |         array          | ObjectId`user`  |

## Review For Destinations Details

|   \_id   |   name   |  review  |  rating  |  destination_id  |  user_id  |
| :------: | :------: | :------: | :------: | :--------------: | :-------: |
| ObjectId |  string  |  string  |  number  |     ObjectId`destination`     | ObjectId`user`  |

