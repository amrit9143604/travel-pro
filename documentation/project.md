## Tech Stack - Frontend

![Nextjs](https://img.shields.io/badge/Nextjs-%23E7EEF0.svg?style=for-the-badge&logo=next.js&logoColor=black)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)

## Tech Stack - Backend 

![Nodejs](https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white)
![ExpressJs](https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB)
![Javascript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E)
![MongoDB](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)



## Frontend 

### Home Page
- **Buttons:**
  - **Login:** Redirects regular users to the login page.
  - **Signup:** Takes users to the signup page to create a new account.
  - **Admin Login:** Redirects admins to the admin login page.

### Signup Page
- **Fields:**
  - **Username:** Input field for user's desired username.
  - **Email:** Input field for user's email address.
  - **Password:** Input field for user's password.

### Login Page
- **Fields:**
  - **Username/Email:** Input field for the user's username or email.
  - **Password:** Input field for the user's password.

### Admin Login Page
- **Fields:**
  - **Email:** Input field for the admin's username or email.
  - **Password:** Input field for the admin's password.

### Sidebar Options for Users
1. **Dashboard:** View user-specific data and information.
2. **Explore Maps:** Discover new destinations through maps.
3. **Explore Itineraries:** Browse through existing itineraries for travel inspiration.
4. **Itinerary:** View and manage personal itineraries.
5. **Saved Destinations:** Access a list of saved destinations for future reference.
6. **Saved Itineraries:** Access a list of saved itineraries for future trips.
7. **See Experiences:** Explore user-generated content and experiences.
8. **Logout:** Sign out of the user account.

### Sidebar Options for Admins
1. **Add Destination:** Add new destinations to the platform.
2. **Add Itinerary:** Create and manage travel itineraries.
3. **Logout:** Sign out of the admin account.


### Features

- **ChatBot:** Integrated chatbot for travellers to get the answers for queries related to travel.

- **Destination Search:** Users can search for the destinations according to keywords such as city, landmarks, towns etc, this is done using MongoDB Atlas Search which is optimized for handling large number of users.

- **Destination Details:** Provide comprehensive information about each location, such as well-known landmarks, typical trip costs, and meteorological conditions.

- **Itinerary Planner:** Travellers can design and alter their own itineraries by including destinations and events they would like to see on particular days.

- **User Authentication:** Permit users to register and access their favourite places and itineraries.

- **User Dashboard:** Give users access to a customised dashboard where they can browse through their favourite locations, saved itineraries, and top travel destinations.

- **Reviews and Ratings:** Permit users to rate and share and experience the places and activities they've been to.

- **Social Sharing:** Give people the ability to share their planned travels with friends and family on social media.


## Backend

### Endpoints

### Admin

##### Admin Authentication

* ```POST /api/v1/admin/signin``` - Admin Login.

* ```POST /api/v1/admin/signup``` - Admin Signup.

* ```POST /api/v1/admin/signout``` - Admin Signout.

##### Admin Destination

* ```POST /api/v1/admin/add-destination``` - Add a new destination to the portal.

* ```POST /api/v1/admin/delete-destination``` - Delete a destination from the portal.

* ```POST /api/v1/admin/update-destination``` - Update a destination from the portal.

* ```POST /api/v1/admin/get-all-destination``` - Get all the destinations from the portal.

* ```POST /api/v1/admin/get-destination``` - Get a destination from the portal.

* ```POST /api/v1/admin/upload-files``` - Upload images of the destinations to s3.

* ```POST /api/v1/admin/delete-files``` - Delete images of the destinations from s3.

##### Admin Itinerary

* ```POST /api/v1/admin/add-itinerary``` - Add a new itinerary to the portal.

* ```POST /api/v1/admin/delete-itinerary``` - Delete an itinerary from the portal.

* ```POST /api/v1/admin/update-itinerary``` - Update an itinerary from the portal.

* ```POST /api/v1/admin/get-all-itinerary``` - Get all the itineraries from the portal.

* ```POST /api/v1/admin/get-itinerary``` - Get a itinerary from the portal.


#### User

##### User Authentication

* ```POST /api/v1/users/signup``` - User Signup

* ```POST /api/v1/users/signin``` - User Login

* ```POST /api/v1/users/signout``` - User Signout

##### User Destination

* ```POST /api/v1/users/get-destination``` - Get a particular destination using the destination_id.

* ```GET /api/v1/users/get-all-destinations``` - Get all in an array of destinations.

* ```POST /api/v1/users/save-destination``` - Save a destination to your profile with a toggle functionality.

* ```POST /api/v1/users/get-saved-destinations``` - Get all the saved destinations of a user.

##### User Review

* ```POST /api/v1/users/add-review``` - Add a review for a destination.

##### User Itinerary

* ```POST /api/v1/users/add-itinerary``` - Create a new itinerary by a user.

* ```POST /api/v1/users/update-itinerary``` - Update an itinerary created by the user.

* ```POST /api/v1/users/delete-itinerary``` - Delete an itinerary created by the user.

* ```POST /api/v1/users/get-itinerary``` - Get a particular itenery.

* ```POST /api/v1/users/get-all-itinerary``` - User can get all the itineraries created by him/her.

* ```POST /api/v1/users/save-itinerary``` - Save an itenary to your profile with a toggle functionality.

* ```POST /api/v1/users/get-saved-itineraries``` - Get all the saved itineraries of a user.

* ```POST /api/v1//users/get-all-admin-itinerary``` - Get all the admin itineraries.

* ```POST /api/v1//users/get-admin-itinerary-data``` - Get a particular admin itinerary.


### Features

- Using JWT tokens for user authentication.

- Bcrypt is used for password hashing.

- Atlas Search, which allows users to utilise keywords to look for locations and events.

- Role-based authentication destinations are listed via admin APIs.

- Shareable, customisable itinerary planner for users.

- For the locations, users are able to add ratings and reviews.

## Testing

- Jest (frontend) 
- Chai and Mocha (backend)

## Deployment

- AWS Amplify (frontend)
- Render (backend)



